include:
  - template: Code-Quality.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - component: $LIBRICIEL_CATALOG_PATH/software-release/publish-compose@2.0.4

stages:
  - build
  - test
  - quality
  - deploy
  - release

variables:
  CONTAINER_IMAGE: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}"
  REGISTRY_CONTAINER_PATH: /public/citoyens/idelibre
  COMPOSE_FILE_NAME: compose.yaml


build:
  stage: build
  tags:
    - citoyen-shell
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /nocache/
  script:
    - docker login -u "gitlab-ci-token" -p "$CI_JOB_TOKEN" $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --target dev --pull -t ${CONTAINER_IMAGE} -f docker/app/Dockerfile .
    - docker login -u "gitlab-ci-token" -p "$CI_JOB_TOKEN" $CI_REGISTRY
    - docker push ${CONTAINER_IMAGE}
    - docker push $CI_REGISTRY_IMAGE:latest

code_quality:
  tags:
    - cq-sans-dind

container_scanning:
  tags:
    - runner-citoyen
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH

phpTest:
  stage: test
  image: $CONTAINER_IMAGE
  services:
    - name: postgres:13-alpine
      alias: postgresidelibre
      command: [ "postgres", "-N", "1000" ]
  variables:
    XDEBUG_MODE: coverage
    APP_ENV: test
    POSTGRES_HOST: postgresidelibre
    POSTGRES_VERSION: 13
    POSTGRES_DB: idelibre_test
    POSTGRES_USER: idelibretest
    POSTGRES_PASSWORD: idelibretest
    POSTGRES_PORT: 5432
    DATABASE_URL: postgresql://idelibretest:idelibretest@postgresidelibre:5432/idelibre?serverVersion=13&charset=utf8
    GIT_STRATEGY: none
    COMPOSER_PROCESS_TIMEOUT: 1200
    ALLOWED_PROFILER_IP: 127.0.0.0
    URL: idelibre-test.libriciel.fr
  before_script:
    - cd /app
  script:
    - SYMFONY_DEPRECATIONS_HELPER=disabled /app/bin/phpunit --coverage-text --colors=never --coverage-clover coverage/coverage.xml --log-junit coverage/logfile.xml
    - cp -rf coverage $CI_PROJECT_DIR
  artifacts:
    paths:
      - coverage
    expire_in: 1h

quality:
  stage: quality
  only:
    - master
  image: gitlab.libriciel.fr:4567/docker/sonar-scanner:latest
  script:
    - mkdir /app
    - cp -rf $CI_PROJECT_DIR/* /app
    - cd /app
    - /sonar-scanner -Dsonar.login=$SONAR_LOGIN -Dsonar.host.url=$SONAR_HOST_URL


publish:
  stage: deploy
  only:
    - tags
  variables:
    IMAGE_VERSION: "$CI_COMMIT_REF_NAME"
    REGISTRY_CONTAINER_PATH: ""
    COMPOSE_FILE_NAME: compose.yaml
  tags:
    - docker-build
  script:
    - echo "Création de l'image ${REGISTRY_CONTAINER_HOST}/public/citoyens/idelibre:${IMAGE_VERSION}"
    - echo ${IMAGE_VERSION} > VERSION
    - docker login -u "$REGISTRY_CONTAINER_USER" -p "$REGISTRY_CONTAINER_PASSWORD" "$REGISTRY_CONTAINER_HOST"
    - docker build --pull -t ${REGISTRY_CONTAINER_HOST}/public/citoyens/idelibre:${IMAGE_VERSION} -f docker/app/Dockerfile .
    - docker push ${REGISTRY_CONTAINER_HOST}/public/citoyens/idelibre:${IMAGE_VERSION}
    - docker build --pull -t ${REGISTRY_CONTAINER_HOST}/public/citoyens/nginx-idelibre:${IMAGE_VERSION} -f docker/frontal/Dockerfile .
    - docker push ${REGISTRY_CONTAINER_HOST}/public/citoyens/nginx-idelibre:${IMAGE_VERSION}



deploy_master:
  stage: release
  environment:
    name: idelibre-4-4.dev.libriciel.net
    url: https://idelibre-4-4.dev.libriciel.net
  tags:
    - citoyen-shell
  only:
    - master
  variables:
    COMPOSE_FILE_NAME: compose.yaml
    PAYLOAD: "payload={\"text\": \"[idelibre-4-4.dev.libriciel.net](https://idelibre-4-4.dev.libriciel.net) a été mis à jour - [build $CI_PIPELINE_ID]($CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID)\"}"
  script:
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$IDELIBRECI_SSH_KEY")
    - mkdir -p ~/.ssh
    - 'echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - sed -i -e"s|TAG|canary|" compose.yaml
    - scp ./compose-deploy.yaml idelibreci@idelibre-4-4.dev.libriciel.net:/opt/idelibre/dist/compose.yaml
    - echo "$MASTER_ENV_FILE_CONTENT" > /tmp/.env.dist
    - scp /tmp/.env.dist idelibreci@idelibre-4-4.dev.libriciel.net:/opt/idelibre/dist/.env
    - ssh -tt idelibreci@idelibre-4-4.dev.libriciel.net "cd /opt/idelibre/dist/  && docker compose pull && ./initializeCertificatesDeploy.sh -f"
    - curl -i -X POST -d "$PAYLOAD" $MATTERMOST_WEBHOOK



publish-compose:
  needs: [ "publish" ]
  variables:
    COMPOSE_PATH: ./compose.yaml
    PRODUCT_NAME: idelibre
  before_script:
    - sed -i "s/VERSION$/${CI_COMMIT_REF_NAME}/" compose.yaml
