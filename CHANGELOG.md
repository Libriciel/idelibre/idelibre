# Changelog
All notable changes to this project will be documented in this file.

## [4.4.0] - 2025-07-31
### Evolution
- Une fois la séance archivé, les données affichées dans le rapport sont celles émisent au moment de l'archivage

### Correction

## [4.3.9] - 2025-01-10
### Evolutions
- Mise à jour docker

### Modifications
- Ajout d'un container permettant de récupérer la version de l'app


## [4.3.8] - 2025-01-06

### Evolutions
- Symfony 6.4.17
- Mise a jour de sécurité 

### Modifications
- [Expéditeur] Modification des règles de validation. L’expéditeur doit être composé uniquement de caractères alphabétiques (lettres) ou alphanumériques (lettres et chiffres). Il ne doit contenir ni espaces ni caractères spéciaux.

## [4.3.7] - 2024-10-31

### Evolutions
- Evolution symfony 6.4.13
- MAJ dependances npm

## [4.3.6] - 2024-10-16
### Correction
- Fix Manque la base url du message de présence

## [4.3.5] - 2024-10-16

### Correction
- Orthographe (#667)
- Fix erreur de baseUrl pour le lien mot de passe oublié depuis un client (web ou mobile)
- Fix a les type booléens avec une valeur null en bdd



## [4.3.4] - 2024-09-27

### Evolution
- Symfony 6.4.12
- Message d'alerte si l'url est idelibre-api.fr
- Ajout d'un champ "Commune" lors de la création d'un élu. (non obligatoire)
- Champs "Commune" est visible lors de la création/édition d'un élu.
- L'import et l'export CSV des utilisateurs intègre le champ "Commune".
 




## [4.3.3] - 2024-09-17

### Evolution
- Possibilité de transformer un élu en suppléant #648
- Ajout du name dans le docker compose #679

### Correction
- Bug : suppression des tokens en local sur le client si on ajoute un compte avec une erreur #675.
- Bug : La fonctionnalité de renvoi de l'email de convocation met à jour la date d'envoi #677
- Bug graphique : #678
- Bug : la preview de l'email des suppléants montre l'email d'invitation.


## [4.3.2] - 2024-09-04

### Evolution
- Mise à jour SF 6.4.11, des dépendances composer.

### Correction
- Securité : Cacher la version du server nginx dans les headers.
- Securité : Limiter l'acces aux mode debug
- Securité : Mise à jour de nginx en 1.27


## [4.3.1] - 2024-08-30

### Evolution
- Mise à jour SF 6.4.10, des dépendances composer.
- La notice RGPD est consultable sans etre connecté.

### Correction
- Correction fautes d'orthographe.
- Correction crash reinitialisation mot de passe administrateur de groupe


## [4.3.0] - 2024-07-26
### Evolution
- Possibilité de définir un titre pour les annexes.
- Export des utilisateurs en CSV par structure et par groupe de structure.
- Les "autres documents" sont affichés dans le PDF complet de la séance.
- Les admins peuvent désormais invalider les mots de passe des utilisateurs, y compris le leur, via la fonction "invalider mot de passe".
- Nouveau rôle ajouté : "Suppléant" lors de la création d'un utilisateur.
- Possibilité de convoquer les suppléants.
- Possibilité d'assigner un suppléant et de le désigner comme remplaçant pour une séance.
- Possibilité de donner procuration à un élu pour une séance.
- Ajout d'un connecteur vers l'application Lsvote.
- Bouton d'envoi de la séance vers Lsvote.
- Importation des résultats de vote depuis Lsvote en PDF ou JSON.
- Ajout d'un décompte des élus n'ayant pas donné de réponse à la convocation en cas d'envoi de la séance vers l'application Lsvote.
- Ajout d'une sécurité lors de la suppression d'une structure, d'un utilisateur ou groupe d'utilisateurs, et de l'invalidation de mot de passe.
- Nouveau mode de connexion simplifié pour l'accès aux séances.
- Lien de connexion magique pour l'accès à une séance sans mot de passe.
- Consultation des jetons d'horodatage avec vérification de la validité depuis Idelibre.
- Ajout de la possibilité de consulter le journal d'événements.
- Possibilité de renvoyer l'email de convocation.


### Correction
- fix un bug sur la recherche d'un admin
- fix un bug de style sur la creation d'un template d'email
- fix bug lors de la suppression d'un import pdf du fichier d'invitation
- Bug d'ouverture des annexes depuis la visionneuse ios.
- Mise à jour des dépendances.
- Mise à jour symfony 6.4.9


## [4.2.23] - 2024-07-31

### Evolution
- Mise à jour SF 6.4.10, des dépendances composer
- La notice RGPD est consultable sans etre connecté.


## [4.2.22] - 2024-07-18

### Evolution
- Mise à jour SF 6.4.9, des dépendances composer et npm jour.

## [4.2.21] - 2024-03-27

### Evolution
- Mise à jour SF 6.4.5, des dépendances composer et npm jour


## [4.2.20] - 2023-12-19

### Correction
- Correction du timeout lorsque le temps d'envoi et de traitement de comelus est trop long (idle timeout)


## [4.2.19] - 2023-12-15

### Correction
- Traitement de nouveaux caractères dans les noms de fichiers lors de l'envoi via comelus


### Evolution
- Ajout d'un script de purge des PDFs complets et ZIP des séances pour toutes les structures




## [4.2.18] - 2023-12-06

### Evolution
- Mise à jour SF 6.4.1, des dépendances composer et npm

### Correction
- Si la séance dépasse la limite maximum pour la génération du PDF complet et du zip, les anciens fichier générés sont supprimés (#484)


## [4.2.17] - 2023-12-01

### Evolution
- Procédure d'installation normalisée pour la compatibilité avec le script libriciel-update.sh
- Mise à jour SF 6.4.0 et des dépendances composer



## [4.2.16] - 2023-11-23

### Evolution
- Mise à jour symfony 6.3.8
- Mise à jour des dépendances composer

### Correction
- InitializeCertificate utilise maintenant docker compose et plus docker-compose
- Envoi à comélus ne se fait pas si le zip généré fait plus de 200 mo

## [4.2.15] - 2023-10-30
### Evolution
- Mise à jour symfony en 6.3.7
- Mise à jour composer et npm

### Correction
- L'invalidation des mots de passe retournait une erreur (#475)
- L'administrateur peut désormais invalider des mots de passe (tout comme le superadministrateur et l'administrateur de groupe)(#476)

## [4.2.14] - 2023-09-22
### Evolution
- Mise à jour symfony en 6.3.4
- Mise à jour composer et npm

### Correction
- Correction des droits d'accès à l'invalidation des mots de passe (#462)
- Correction du delai de purge des données pour la durée jamais

## [4.2.13] - 2023-09-18
### Correction
- Dans le cas où le poids de l'ensemble des projets dépasse la limite fixée, le PDF complet et le ZIP de la séance ne sont pas générés, mais les liens permettant de les télécharger restent actifs.
- Au moment où on définit un modèle d'email au format HTML, le sanitizer faisait disparaître les balises style

## [4.2.12] - 2023-08-02
### Correction
- La purge des séances ne fonctionnait pas à cause d'une erreur dans la configuration de la durée par défaut

## [4.2.11] - 2023-08-02
### Evolution
- Mise à jour symfony en 6.3.3

### Correction
- Les utilisateurs possédant un profil Administrateur ne pouvaient plus être modifiés

## [4.2.10] - 2023-07-18
### Correction
￼- L'entropie du mot de passe des profils "Administrateur" était celui défini par la structure au lieu du niveau maximum (130).
- Désormais, il n'est plus possible de définir le mot de passe pour les profils "Administrateur", qui ont donc par défaut un niveau d'entropie de 130.

## [4.2.9] - 2023-07-06
### Correction
￼- Suite au passage en Symfony 6.3.1, le breadcrumb ne transformait plus les variables définies dans les controllers (données dans le fil d'ariane).

## [4.2.8] - 2023-06-29
### Evolution
- Mise à jour symfony en 6.3.1
- suppression de la lib sensioExtraFramework dépréciée
- Mise à jour des lib npm

## [4.2.7] - 2023-06-28

### Correction
￼- Le fuseau horaire contenu dans le fichier ics envoyé dans le mail de la séance présentait un décalage d'1 heure selon certain logiciel de gestion d'agenda

## [4.2.6] - 2023-06-02

### Correction
￼- Le chargement de certains PDFs posent soucis si leur contenu ne finit pas par un EOF

## [4.2.5] - 2023-06-02

### Correction
￼- Le téléchargement des autres documents n'était pas possible


## [4.2.4] - 2023-05-19

### Correction
- Une erreur dans le docker-compose bloquait le memory_limit d'idelibre à la valeur minimale (128M)

## [4.2.3] - 2023-05-16

### Evolution
- nginx passage de la version 1.17 vers 1.23

### Correction
- Contournement du openssl posant souci lors de l'envoi des mails avec ajout du verify_peer=false

## [4.2.2] - 2023-05-15

### Evolution
- docker compose restart: unless-stopped

### Correction
- Bug client web crash après mise à jour (otherdoc null)

## [4.2.1] - 2023-05-05

### Evolution
- Mise à jour symfony en 6.2.10
- Ajout des fonctionnalités manquantes dans l'api :
    - Séance autorisée à distance
    - Ajout de documents non numérotés

### Correction
- Script de mise à jour initBdd:subscription_user trop long.
- Correction docker-compose.yml au niveau du tag de node-idelibre

## [4.2.0] - 2023-03-31

### Evolution
- Changement de version de nginx en 1.23
- Docker passage  en Ubuntu 22:04
- Node 12 -> 18
- php 8 -> php8.1
- Concatenation -> pdftk to pdfunite
- Gestion des fichiers 'encrypted' avec qpdf
- Mise en place d'un système permettant de bloquer le brut force
- Mise en place du calcul d'entropie sur les mot de passes
- Un mail d'initialisation du mot de passe est envoyé lors de la création d'un compte superadmin et admin de groupe.
- Délai de purge de donnée configurable par entité (#256)
- Lors de la création d'un utilisateur dans une structure, il existe deux possiblité concernant le mot de passe :
    - soit un mail d'initialisation du mot de passe est envoyé
    - soit il est possible de définir le mot de passe
- Mise en place d'envoi d'un mail de réinitialisation du mot de passe.
- Mise en place de l'invalidation des mots de passe de tous les utilisateurs de la structure.
- Empêcher IL de générer les pdfs complets s'il y a trop de volume (#290)
- Amélioration du nommage des fichiers lors de l'envoi à comelus et ajout d'un zip complet de la séance téléchargeable (#276)
- Rassemblement des projets et autres documents sur la même page (#336)
- Ajout des "autres documents" dans le zip de la séance (#336)
- Amélioration du nommage des fichiers lors de l'envoi à comelus et ajout d'un zip complet de la séance téléchargeable (#276)
- Symfony 6.2
- Remplacement du composant déprécié Security par le nouveau

### Correction
- la suppression d'une structure supprime bien tout ce qui lui est associée
- La suppression d'une séance supprime aussi le fichier d'invitation


### Suppression
- Lors de la création / modification d'un admin de group ou d'un superadmind, il n'est plus possible de définir le mot de passe du nouveau utilisateur.
- Lors de la modification d'un admin de group ou d'un superadmind, il n'est plus possible de changer l'adresse e-mail de l'utilisateur.

## [4.1.4] - 2022-08-31

### Evolution
- symfony 5.4.12
- possibilité de cacher la bannière de gauche de l'écran de login

### Correction
- Erreur d'adresse et de siret de l'éditeur dans la notice rgpd


## [4.1.3] - 2022-07-22

### Evolution
- symfony 5.4.10

### Correction
- Erreur minimum.sql lors d'une fresh install

## [4.1.2] - 2022-03-23

### Evolution
- symfony 5.4.6
- Fonctionnalité de stats iso 3.x


## [4.1.1] - 2022-03-18

### Evolution
- Script de purge
- script de creation des repertoires de token post migration
- Récupération de l'api32


### Correction
- Envoi de sms par lot via lsmessage (#264)

## [4.1] - 2022-03-01
### Evolution
- nouveau systeme d'authentification
- Php8
- Dans modele de mail replacer la denomination balises par variables (#165)
- Les utilisateurs personnel administratif et invité peuvent etre crée via csv (#162)
- La civilité peut etre renseignée via csv (#161)
- Un admin de groupe ne peut plus supprimer une structure (#160)
- Depuis le client web Nommage plus explicite  du fichier zip de la séance (#154)
- Ajout de la reecriture de l'host dans le vhost
- Suppression de la limite du nombre de carractere dans la description du connecteur comelus (#176)
- Champs de publipostage disponible dans la description du connecteur comelus (#164)
- Client web, nouveau logo idelibre
- Afficher / masquer le mot de passe (#84)
- Alerte si jamais le navigateur ne permet pas de télécharger les fichiers (#172)
- Politique de confidentialité configurable par structure (#113)
- Assignation de bon onwer sur le repertoire file monté sur le docker si il n'est pas correcte (#178)
- Renommage des zip et pdf complet de la seance au format : nomSeance_DateSeance (#192)
- [Technique] remplacement de swift_mailer par mailer (#194)

### Nouveautés
- Fichier de rendez vous avec l'envoi du mail de convocation (#152)
- Jeton d'horodatage lors de l'ajout de document dans une séance déja envoyée (#156)
- Apiv2 (avec recupération des jetons d'horodatage)
- Gestion des clés d'api
- Ajout des thèmes via csv (#179)
- Mot de passe oublié client web (#158)


### Correction
- Format de la date lors de la création et modification d'une séance (#167)
- mise à jour du nom complet des sous themes lors de la mise à jour du nom d'un theme (#159)
- Docker : Creation des repertoires au demarage s'ils n'existent pas (#178)
- supression de l'ancienne orthographe d'idelibre (#247)
- Reception du mail de convocation 2 fois (#246)
- Dupplicata d'accusé de réception (#244)
- Ordre chronologique navigateur (#235)
- tout Type de séance visible pour gestionnaire dans le tableau de bord (#232)
- desactivation du connecteur comelus (#232)
- Enregistrement de la liste de difusion comelus (#225)
- Connecteur ls message : message d'erreur si champs expéditeur supérieur à 11 caractères (#210)

## [4.0.6] - 2021-10-04
### Evolution
- Nombre de fichiers maximum par envoi est maintenant de 600 (#151)
- Temps maximum d'execution d'un script est de 300s
- symfony 5.3.9 (conservation de doctrine orm en 2.9 pour eviter erreur sur les arbres)

### Correction
- Impossibilité d'ajouter 2 fois de suite le meme fichier sous chrome et edge (#150)
- correction de la source du fichier d'initialisation des certificats

## [4.0.5] - 2021-09-16
### Evolution
- Connecteur legacy : accepte les array et les string pour la clé acteurs-convoqués
- Connecteur legacy : Si un username généré existe déja on considère que c'est le même élu
- Redirections des anciennes routes vers les nouvelles pour eviter les 404 en favoris (srvusers/login et idelibre_client3)



## [4.0.4] - 2021-09-06
### Correction
- L'aperçu de l'email de template en format text echappe maintenant les balises html (#132)
- Correction de l'impossibilité de supprimer un acteur s'il a une annotation (#137)
- Selectionner tous les élus dans l'association au type (#136)
- Suppression par lot (#135)
- Fautes d'orthographes (#133)
- Les balises html sont visibles dans l'appercu des templates d'email si on est en mode texte (#132)


### Evolutions
- Déarchiver une séance (#138)
- Si on ajoute une invitation apres l'enregistrement cela ajoute bien les administratifs et invites par defaut (#130)
- Label des champs fichiers dans la création d'une séance (#134)
- Mise à jour symfony 5.3.7


## [4.0.3] - 2021-08-27
### Evolutions
- mise en place de sentry
- enlever le suffix dans le select de l'association des utilisateurs aux types (#125)
- rendre configurable si un groupe peut creer ou non des structures (#127)
- rendre le connecteur WD non snessible à la casse et aux espaces en périphérie (#126)



## [4.0.2] - 2021-08-25

### Corrections
- impossibilité pour un gestionnaire de seance de creer une seance (erreur 500)
- impossibilité de supprimer un utilisateur rapporteur d'un projet (erreur 500)

### Evolutions
- Mise à jour symfony 5.3.6

## [4.0.1] - 2021-08-24

### Evolutions
- Mise à jour du docker-compose

### Corrections
- Correction du calcul du hash du legacy password coté client (nodejs)
