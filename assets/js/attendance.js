import axios from "axios";

const attendanceInput = document.querySelector('#attendance_attendance');
const mandataireGroup = document.querySelector('#attendance_mandataire_group');
const mandataireInput = document.querySelector('#attendance_mandataire');
const deputyGroup = document.querySelector('#attendance_deputy_group')
const deputyInput = document.querySelector("#attendance_deputy")
const insertAttendanceHere = document.querySelector('#confirmation-presence-actor')
const attendanceActor = document.querySelector('#attendance-actor')
const popupBtn = document.querySelector('#confirm-attendance-btn')


function getToken () {
    const token = window.location.pathname.split("/")[3]
    return token + ''

}
let url = `/attendance/${getToken()}`

window.onload = () => {
    let attendanceValue = attendanceInput?.value;

    if(attendanceValue === "poa"){
        show(mandataireGroup)
        hide(deputyGroup)
        return;
    }

    if( attendanceValue === "deputy"){
        show(deputyGroup)
        hide(mandataireGroup)
        return;
    }

    hide(mandataireGroup)
    hide(deputyGroup)
}

attendanceInput.onchange = () => {
    let attendanceValue = attendanceInput?.value;

    if(attendanceValue === "poa"){
        show(mandataireGroup)
        hide(deputyGroup)
        return;
    }

    if( attendanceValue === "deputy" ){
        show(deputyGroup)
        hide(mandataireGroup)
        return;
    }

    hide(mandataireGroup)
    hide(deputyGroup)
}
document.addEventListener('click', (e) => {

    if(e.target.id === "confirm-attendance-btn"){
        let attendanceValue = attendanceInput?.value;

       switch (attendanceValue) {
           case "present":
               insertAttendanceHere.innerHTML = "présent"
               break;
           case "remote":
               insertAttendanceHere.innerHTML = "présent à distance"
               break;
           case "poa":
               insertAttendanceHere.innerHTML = `donner pouvoir au mandataire ${mandataireInput.options[mandataireInput.selectedIndex].text} via une procuration `
               break;
           case "deputy":
               insertAttendanceHere.innerHTML = `être remplacé par le suppléant : ${deputyInput.options[deputyInput.selectedIndex].text} `
               break;
           default:
               insertAttendanceHere.innerHTML = "absent"
               break;
       }
    }

})









