import $ from "jquery";
$('document').ready(function () {
    let fieldMessage = $('.sms_count_message'),
        nbMaxCharacters = $(fieldMessage).attr('data-nb-max-characters') || 160,
        messageVal = $(fieldMessage).val();

    if (messageVal) {
        countCharactersMessage(messageVal, nbMaxCharacters);
    }

    let senderVal = $('.sms_count_sender').val();
    if (senderVal) {
        countCharactersSender(senderVal, $('.sms_count_sender').attr('maxlength'));
    }
});

window.countCharactersMessage = (textMessage, nbMaxMessage) =>  {
    let nbTextMessage = textMessage.length,
        elementMessage = $('.sms_count_message'),
        elementMessageHelp = $('#'+$(elementMessage).attr('id')+'_help')
    ;
    if (nbTextMessage > nbMaxMessage) {
        $(elementMessage).val(getOldText(textMessage));
        updateMessageInfo( $(elementMessageHelp), (nbTextMessage-1), nbMaxMessage);
        messageAlert();
    } else {
        updateMessageInfo( $(elementMessageHelp), nbTextMessage, nbMaxMessage);
    }
}

window.countCharactersSender = (textSender, nbMaxSender) =>  {
    let nbTextSender = textSender.length,
        elementSender = $('.sms_count_sender'),
        elementSenderHelp = $('#'+$(elementSender).attr('id')+'_help')
    ;

    if (nbTextSender > nbMaxSender) {
        $(elementSender).val(getOldText(textSender));
        updateMessageInfo( $(elementSenderHelp), (nbTextSender-1), nbMaxSender);
        messageAlert();
    } else {
        updateMessageInfo( $(elementSenderHelp), nbTextSender, nbMaxSender);
    }
}

function getOldText(text)
{
    return text.substring(0, text.length - 1);
}

function updateMessageInfo(field, nbText, nbMax)
{
    $(field).text(nbText + ' / ' + nbMax + ' caractères');
}
function messageAlert()
{
    alert('Attention, vous avez dépassé le nombre de caractère autorisé');
}
