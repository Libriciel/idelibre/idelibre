import 'pdfjs-dist/build/pdf.worker.mjs'
import * as pdfjsLib from "pdfjs-dist";
import * as pdfjsViewer from "pdfjs-dist/legacy/web/pdf_viewer.mjs";

pdfjsLib.GlobalWorkerOptions.workerSrc = 'pdfjs-dist/pdf.worker.min.mjs';

document.addEventListener('DOMContentLoaded', function () {
    const pdfContainer = document.getElementById('pdf-container');
    const url = pdfContainer.getAttribute('data-url');

    const pageNumberInput = document.getElementById('page-number');
    const TotalNumberSpan = document.getElementById('total-page-number');
    const rotateButton = document.getElementById('rotate-pdf');
    const zoomInButton = document.getElementById('zoom-in');
    const zoomOutButton = document.getElementById('zoom-out');

    let maxPage = 1;

    const eventBus = new pdfjsViewer.EventBus();
    let pdfLinkService = new pdfjsViewer.PDFLinkService({
        eventBus: eventBus,
    });


    const pdfViewer = new pdfjsViewer.PDFViewer({container: pdfContainer, eventBus, linkService: pdfLinkService});


    pdfLinkService.setViewer(pdfViewer);


    const loadingTask = pdfjsLib.getDocument({
        url: url,
    });


    function initializeDocument(pdfDocument) {
        pdfViewer.setDocument(pdfDocument);
        pdfLinkService.setDocument(pdfDocument, null);
        console.log(pdfDocument.numPages);
        maxPage = pdfDocument.numPages;
        TotalNumberSpan.textContent = maxPage;
        pageNumberInput.value = 1;
    }


    loadingTask.promise.then(function (pdfDocument) {
        initializeDocument(pdfDocument);

    }).catch(function (reason) {
        console.error(`Error: ` + reason);
    });


    eventBus.on("pagechanging", function (pagechangingEvent) {
        console.log(pagechangingEvent.pageNumber);
        pageNumberInput.value = pagechangingEvent.pageNumber

        console.log('sdsdsds');

    })


    pageNumberInput.addEventListener('change', () => {
        const pageNumber = parseInt(pageNumberInput.value, 10);
        if (pageNumber > 0 && pageNumber <= maxPage) {
            pdfViewer.currentPageNumber = pageNumber;
        }
    });


    rotateButton.addEventListener('click', () => {
        pdfViewer.pagesRotation += 90;
    });

    zoomInButton.addEventListener('click', () => {
        pdfViewer.currentScale += 0.1;
    });

    zoomOutButton.addEventListener('click', () => {
        pdfViewer.currentScale -= 0.1;
    });



    eventBus.on("pagesinit", function () {
        pdfViewer.currentScaleValue = "page-width";
        console.log('pagesinit');
    })

    window.addEventListener('pagechange', function (e) {
        if (e.pageNumber !== e.previousPageNumber) {
            console.log('page changed from ' + e.previousPageNumber + ' to ' + e.pageNumber);
        }
    });

    window.addEventListener('resize', () => {
        console.log(pdfViewer);
        pdfViewer.currentScaleValue = "page-width";
    });

    window.changePdf = url => {
        const loadingTask = pdfjsLib.getDocument({
            url: url,
        });

        loadingTask.promise.then(function (pdfDocument) {
            initializeDocument(pdfDocument);
        }).catch(function (reason) {
            console.error(`Error: ` + reason);
        });
    }

});
