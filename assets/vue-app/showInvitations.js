import './vue-app.css';
import Vue from 'vue/dist/vue';
import axios from 'axios';

Vue.filter('formatDateString', function (value, timezone) {
    if (value && timezone) {
        const date = new Date(value);
        const tz = date.toLocaleString("utc", {timeZone: timezone});
        return tz.substring(0, tz.length - 3)
    }
});

let app = new Vue({
    delimiters: ['${', '}'],
    el: "#app",
    data: {
        comelusId: null,
        isComelus: false,
        isComelusSending: false,
        lsvoteId: null,
        actorConvocations: [],
        guestConvocations: [],
        employeeConvocations: [],
        deputyConvocations: [],
        deputiesAvailable: [],
        isAlreadySentActors: false,
        isAlreadySentGuests: false,
        isAlreadySentEmployees: false,
        isArchived: true,
        showModalComelus: false,
        showModalSend: false,
        previewUrl: "",
        previewData: "",
        previewSubject: "",
        showModalMailExample: false,
        filter: {
            actor: "",
            guest: "",
            employee: ""
        },
        showModalAttendance: false,
        attendanceStatusActors: [],
        attendanceStatusEmployee: [],
        attendanceStatusGuest: [],
        changedAttendance: [],
        errorMessage: null,
        infoMessage: null,
        showModalNotifyAgain: false,
        notification: {
            object: "",
            content: "",
        },
        convocationIdCurrent: "",
        isInvitation: false,
        timezone: "",
        options: "",
        actorsInSitting : [],
        notAnswered: 0,
        inTheRoom: '',
        assignedAndPresentDeputies: '',
        quorum: '',
        poa: '',
    },

    computed: {
        filteredActorConvocations: function () {
            return filter(this.actorConvocations, this.filter.actor);
        },

        filteredEmployeeConvocations: function () {
            return filter(this.employeeConvocations, this.filter.employee);
        },

        filteredGuestConvocations: function () {
            return filter(this.guestConvocations, this.filter.guest);
        },
        filteredDeputyConvocations: function () {
            return filter(this.deputyConvocations, this.filter.deputy);
        }
    },

    methods: {
        sendConvocation(convocationId) {

            this.showModalSend = true;

            axios.post(`/api/convocations/${convocationId}/send`).then(response => {
                updateConvocations(this.actorConvocations, response.data);
                updateConvocations(this.guestConvocations, response.data);
                updateConvocations(this.employeeConvocations, response.data);
                updateConvocations(this.deputyConvocations, response.data);

                this.showModalSend = false;

                this.isAlreadySentActors = isAlreadySentSitting(this.actorConvocations);
                this.isAlreadySentGuests = isAlreadySentSitting(this.guestConvocations);
                this.isAlreadySentEmployees = isAlreadySentSitting(this.employeeConvocations);

                this.setInfoMessage("La convocation a été envoyée à " + response.data.user.lastName + " " + response.data.user.firstName);
            }).catch((e) => {
                console.log(e.message);
                this.setErrorMessage("Erreur lors de l'envoi");
                this.showModalSend = false;

            });
        },

        sendConvocations(type) {
            let url = `/api/sittings/${getSittingId()}/sendConvocations`;
            if (type === 'Actor') {
                url += "?userProfile=Actor"
            }
            if (type === 'Employee') {
                url += "?userProfile=Employee"
            }

            if (type === 'Guest') {
                url += "?userProfile=Guest"
            }

            this.showModalSend = true;
            axios.post(url).then(() => {
                this.getConvocations();
                this.showModalSend = false;
                this.setInfoMessage("Les convocations ont été envoyées à tous les destinataires");
            }).catch((e) => {
                console.log(e.message);
                this.setErrorMessage("Erreur lors de l'envoi");
                this.showModalSend = false;

            });
        },

        getConvocations() {
            axios.get(`/api/convocations/${getSittingId()}`).then(convocations => {
                this.actorConvocations = convocations.data['actors'];
                this.guestConvocations = convocations.data['guests'];
                this.employeeConvocations = convocations.data['employees'];
                this.deputyConvocations = convocations.data['deputies'] ?? [];

                this.quorum = convocations.data['quorum'];
                this.inTheRoom = convocations.data['inTheRoom'];
                this.poa = convocations.data['poa'];
                this.assignedAndPresentDeputies = convocations.data['assignedAndPresentDeputies'];

                this.isAlreadySentActors = isAlreadySentSitting(this.actorConvocations);
                this.isAlreadySentGuests = isAlreadySentSitting(this.guestConvocations);
                this.isAlreadySentEmployees = isAlreadySentSitting(this.employeeConvocations);
                this.convocationIdCurrent = getConvocationCurrentId(convocations);
            })
        },

        getAvailableDeputies(convocationId) {
            axios.get(`/api/convocation/${convocationId}/deputiesAvailable`).then(response => {
              this.deputiesAvailable = response.data['deputiesAvailable'];
            });
        },

        updateAvailableDeputiesConvocations(convocationId) {

        },


        getSitting() {
            axios.get(`/api/sittings/${getSittingId()}`).then(response => {
                const sitting = response.data;
                this.comelusId = sitting?.comelusId ?? null;
                this.isComelus = sitting?.type.isComelus ?? false;
                this.isArchived = sitting.isArchived;
                this.lsvoteId = sitting?.lsvoteSitting?.lsvoteSittingId;
            })
        },

        getSittingTimezone() {
            axios.get(`/api/sittings/${getSittingId()}/timezone`)
                .then(response => {
                    this.timezone = response.data.timezone
                });
        },

        getCountNotAnswered() {
            axios.get(`/api/sittings/${getSittingId()}/countNotAnswered`)
                .then(response => {
                    this.notAnswered = response.data.notAnswered
                });
        },


        resetFilters() {
            this.filter = {actor: "", guest: "", employees: "", deputy: ""};
        },

        openShowModalNotifyAgain() {
            this.notification.object = ""
            this.notification.content = ""
            this.showModalNotifyAgain = true;
        },

        sendNotifyAgain() {
            axios.post(`/api/sittings/${getSittingId()}/notifyAgain`, this.notification).then(
                (response) => {
                    this.setInfoMessage("Messages envoyés");

                })
                .catch((e) => {
                    console.log(e);
                    this.setErrorMessage("Erreur lors de l'envoi");

                })
                .finally(() => {
                    this.showModalNotifyAgain = false
                });
        },

        sendComelus() {
            this.isComelusSending = true;
            this.showModalComelus = true;
            axios.post(`/api/sittings/${getSittingId()}/sendComelus`, this.notification).then(
                (response) => {
                    this.setInfoMessage("Documents envoyés via comelus");
                    this.comelusId = response.data['comelusId']
                })
                .catch((e) => {
                    console.log(e);
                    this.setErrorMessage("Erreur lors de l'envoi");

                }).finally(() => {
                this.isComelusSending = false;
                this.showModalComelus = false;
            });
        },

        sendLsvote() {
            axios.post(`/api/sittings/${getSittingId()}/sendLsvote`)
                .then((response) => {
                    this.setInfoMessage("Séance envoyée à lsvote");
                    this.lsvoteId = response.data['lsvoteId']
                })
                .catch((e) => {
                    console.log(e);
                    this.setErrorMessage("Erreur lors de l'envoi");
                });
        },

        openShowModalAttendance() {
            this.attendanceStatusActors = formatAttendanceStatus(this.actorConvocations);
            this.attendanceStatusEmployee = formatAttendanceStatus(this.employeeConvocations);
            this.attendanceStatusGuest = formatAttendanceStatus(this.guestConvocations);
            this.showModalAttendance = true;
        },
        resetDeputyAttendanceToAbsent(status){
            this.deputyConvocations.map((convocation) => {
                if (convocation.user.id === status.deputy.id) {
                    axios.get(`/api/convocation/${convocation.id}/resetDeputyAttendance`)
                        .then(response => {
                            console.log("attendance changed to absent")
                        })
                }
            })
        }
        ,


        changeAttendance(status) {
            const attendanceNull = status.attendance === ""
            const attendancePresence = status.attendance === "present"
            const attendanceRemote = status.attendance === "remote"
            const attendanceAbsent = status.attendance === "absent"
            const attendancePoa = status.attendance === "poa"
            const attendanceDeputy = status.attendance === "deputy"

            if( isPoaOrDeputyWithoutDeputy(status) ){
                return;
            }

            if ( attendanceNull || attendancePresence|| attendanceRemote || attendanceAbsent ) {
                if (status.deputy) {
                    this.resetDeputyAttendanceToAbsent(status);
                }

                this.changedAttendance.push({
                    convocationId: status.convocationId,
                    attendance: status.attendance,
                    deputyId: null,
                    mandataireId: null,
                })
                return;
            }

            if (attendancePoa) {
                this.changedAttendance.push({
                    convocationId: status.convocationId,
                    attendance: status.attendance,
                    deputyId:null,
                    mandataireId: status?.mandator?.id,
                })
                return;
            }

            if (attendanceDeputy) {
                this.changedAttendance.push({
                    convocationId: status.convocationId,
                    attendance: status.attendance,
                    deputyId:status?.deputy?.id,
                    mandataireId: null,
                })
            }
        },

        saveAttendance() {
            axios.post(`/api/convocations/attendance`, this.changedAttendance)
                .then(
                (response) => {
                    this.getConvocations();
                })
                .catch((e) => {
                    console.log("erreur : " + e);
                })
                .finally(() => {
                    this.showModalAttendance = false
                    this.changedAttendance = [];
                    this.getCountNotAnswered();
                    return false;
                });

            this.getConvocations();

            },



        setErrorMessage(msg) {
            this.errorMessage = msg
            setTimeout(() => this.errorMessage = null, 3000);
        },
        setInfoMessage(msg) {
            this.infoMessage = msg
            setTimeout(() => this.infoMessage = null, 3000);
        },

        openShowModalMailExample(convocationId, invitation) {
            this.convocationId = convocationId;
            this.previewUrl = `/api/convocations/previewForSecretary/${convocationId}`;
            this.isInvitation = false;
            if (invitation) {
                this.previewUrl = `/api/convocations/previewForSecretaryOther/${convocationId}`;
                this.isInvitation = true;
            }
            this.showModalMailExample = true;
        },

    },


    mounted() {
        this.getSittingTimezone();
        this.getConvocations();
        this.getSitting();
        this.saveAttendance();
        this.getCountNotAnswered();
    }
});


function formatAttendanceStatus(convocations) {
    let status = []
    for (let i = 0; i < convocations.length; i++) {
        let convocation = convocations[i];
        status.push({
            convocationId: convocation.id,
            firstName: convocation.user.firstName,
            lastName: convocation.user.lastName,
            attendance: convocation.attendance,
            deputy: convocation.deputy,
            mandator: convocation.mandator,
            category: convocation.category,
            role: convocation.user.role.name
        })
    }

    return status;
}


function getSittingId() {
    return window.location.pathname.split('/')[3];
}

function getActorId() {
    return window.location.pathname.split('/')[4];
}

function getConvocationCurrentId(convocations) {
    return formatAttendanceStatus(convocations.data['actors'])[0].convocationId;
}

function updateConvocations(convocations, convocation) {
    for (let i = 0; i < convocations.length; i++) {
        if (convocations[i].id === convocation.id) {
            app.$set(convocations, i, convocation)
        }
    }
}

function isAlreadySentSitting(convocations) {
    for (let i = 0; i < convocations.length; i++) {
        if (!convocations[i].sentTimestamp) {
            return false;
        }
    }
    return true;
}


function filter(convocations, search) {

    if (!search || search === "") {
        return convocations
    }

    let filterLowerCase = search.toLowerCase();
    return convocations.filter(convocation =>
        convocation.user.lastName.toLowerCase().includes(filterLowerCase) ||
        convocation.user.firstName.toLowerCase().includes(filterLowerCase) ||
        convocation.user.username.toLowerCase().includes(filterLowerCase)
    )

}

function isPoaOrDeputyWithoutDeputy(status) {
    let isPoaOrDeputy = status.attendance === "poa" || status.attendance === "deputy";
    if(! isPoaOrDeputy) {
        return false;
    }

    return !status.deputy && !status.mandator
}



