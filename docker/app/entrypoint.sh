#!/bin/bash

echo "init databases with symfony"
./bin/console migrate:from40
./bin/console doctrine:migrations:migrate --no-interaction
./bin/console initBdd:subscription_user
./bin/console initBdd:email_template_recap
./bin/console initBdd:connector_lsvote
./bin/console initBdd:add_role
./bin/console migrate:rename_seance_archivee
./bin/console initBdd docker-resources/minimum.sql
./bin/console cache:clear --no-interaction
echo "Starting PHP-FPM"

/usr/local/sbin/php-fpm -F

