#! /bin/bash
# Certbot
if [[ -z "${ENABLE_LETS_ENCRYPT}" ]] ; then
    echo "Let's encrypt not enabled [PASS]"
    exit 0
fi

if [[ -f "/etc/letsencrypt/${SERVER_NAME}/fullchain.pem" ]] ; then
    echo "Try to renew Let's Encrypt certificate for domain ${SERVER_NAME}"
    if ! certbot  --logs-dir /var/log/nginx/ --work-dir /var/run --http-01-port 80 renew; then
        echo "Unable to renew Let's Encrypt certificate [ERROR] ... Continue anyway"
        exit 0
    fi
    cp "/etc/letsencrypt/live/${SERVER_NAME}/fullchain.pem" /etc/nginx/ssl
    cp "/etc/letsencrypt/live/${SERVER_NAME}/privkey.pem" /etc/nginx/ssl
    exit 0
fi

if ! certbot --http-01-port 80 --logs-dir /var/log/nginx/ --work-dir /var/run --standalone --noninteractive --agree-to --preferred-challenges http -d ${SERVER_NAME} -m ${LETS_ENCRYPT_EMAIL} certonly; then
    echo "Failed to create Let's Encrypt certificate [ERROR] ... Continue anyway"
    exit 0
fi

cp "/etc/letsencrypt/live/${SERVER_NAME}/fullchain.pem" /etc/nginx/ssl
cp "/etc/letsencrypt/live/${SERVER_NAME}/privkey.pem" /etc/nginx/ssl
