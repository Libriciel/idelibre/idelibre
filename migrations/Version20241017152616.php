<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241017152616 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('update convocation set is_emailed = false where is_emailed is null');
        $this->addSql('update type set is_sms = false where is_sms is null');
        $this->addSql('update type set is_comelus = false where is_comelus is null');
        $this->addSql('update type set is_sms_guests = false where is_sms_guests is null');
        $this->addSql('update type set is_sms_employees = false where is_sms_employees is null');
    }
}
