<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241028162141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE convocation ADD titular_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN convocation.titular_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE convocation ADD CONSTRAINT FK_C03B3F5FF9F0FF64 FOREIGN KEY (titular_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C03B3F5FF9F0FF64 ON convocation (titular_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE party_legacy_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('ALTER TABLE convocation DROP CONSTRAINT FK_C03B3F5FF9F0FF64');
        $this->addSql('DROP INDEX IDX_C03B3F5FF9F0FF64');
        $this->addSql('ALTER TABLE convocation DROP titular_id');
    }
}
