(function () {
    angular.module('idelibreApp').controller('ModalpresenceCtrl', function ($scope, account, seance, attendance, $rootScope, $modalInstance, socketioSrv, accountSrv, attendanceSrv) {

        let isShowMandators = false;

        if(account.type === ACTEURS){
            isShowMandators = true;
        }


        $scope.isAllowedRemote = seance.isRemoteAllowed

        $scope.isPresenceStatusEditable = seance.getDate() > Date.now();

        $scope.attendance = attendance;

        $scope.isMandatorList = false
        $scope.selectedMandator = null;
        $scope.availableMandators = attendance.availableMandators;

        $scope.isMandatorAllowed = attendance.isMandatorAllowed && isShowMandators;


        $scope.getPresenceMessageEditable = () => {
            switch (attendance.attendance) {
                case Seance.ABSENT :
                    return "Vous êtes actuellement enregistré comme : Absent"
                case Seance.PRESENT :
                    return "Vous êtes actuellement enregistré comme : Présent"
                case Seance.REMOTE :
                    return "Vous êtes actuellement enregistré comme : Présent à distance"
                case Seance.DEPUTY :
                    return "Vous êtes actuellement enregistré comme : Remplacé par son suppléant"
                case Seance.POA :
                    return "Vous êtes actuellement enregistré comme : Donne pouvoir";

                case Seance.undefined :
                    return "Merci de renseigner votre présence"
                default :
                    return "Merci de renseigner votre présence"
            }
        }

        let callBackSuccess = function () {
            $modalInstance.close();
        }

        let callBackError = function () {
            $rootScope.$broadcast('notify', {class: 'danger', content: "Impossible de mettre à jour votre présence"});
            $modalInstance.dismiss('cancel');
        }


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.present = function () {
            attendanceSrv.postAttendanceStatus(account, seance.id, {attendance: Seance.PRESENT}, callBackSuccess, callBackError);
        };

        $scope.presentRemotely = function () {
            attendanceSrv.postAttendanceStatus(account, seance.id, {attendance: Seance.REMOTE}, callBackSuccess, callBackError);
        }

        $scope.absent = function () {
            attendanceSrv.postAttendanceStatus(account, seance.id, {attendance: Seance.ABSENT}, callBackSuccess, callBackError);
        };

        $scope.absentMandator = function () {
            $scope.isMandatorList = true
        }

        $scope.chooseMandator = function () {
            attendanceSrv.postAttendanceStatus(account, seance.id, {attendance: Seance.POA, mandatorId: $scope.selectedMandator}, callBackSuccess, callBackError);
        }

        $scope.absentDeputy = function () {
            attendanceSrv.postAttendanceStatus(account, seance.id, {attendance: Seance.DEPUTY, mandatorId: null}, callBackSuccess, callBackError);
        }
    });

})();
