<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Commande utilisée uniquement pour un passage en v4.3.
 */
#[AsCommand(name: 'migrate:rename_seance_archivee')]
class RenameArchivedSittingEventLogCommand extends Command
{
    public function __construct(private readonly EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('remplace seance archivée par séance classée dans la base de données')
        ;
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        if (!$this->isInit()) {
            $io->text('database is not initialized, nothing to update');

            return 0;
        }

        $io->text('rename Seance Archivée to Seance Classée in event_log table');
        $pdo = $this->entityManager->getConnection()->getNativeConnection();
        $sql = "update event_log set action ='Séance classée' where action = 'Séance archivée'";


        $pdo->beginTransaction();
        try {
            $pdo->exec($sql);
            $pdo->commit();
        } catch (Exception $e) {
            $pdo->rollBack();
            throw $e;
        }

        $io->success('update migration table done');

        return 0;
    }

    private function isInit(): bool
    {
        $pdo = $this->entityManager->getConnection()->getNativeConnection();

        try {
            $pdo->exec('select * from "user"');
        } catch (Exception) {
            return false;
        }

        return true;
    }
}
