<?php

namespace App\Controller\ApiV2;

use App\Entity\Convocation;
use App\Entity\Structure;
use App\Service\Convocation\ConvocationManager;
use App\Service\Email\EmailNotSendException;
use Doctrine\DBAL\ConnectionException;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/v2/structures/{structureId}')]
#[IsGranted('API_AUTHORIZED_STRUCTURE', subject: 'structure')]
class ConvocationController extends AbstractController
{
    /**
     * @throws ConnectionException
     * @throws EmailNotSendException
     */
    #[Route(path: '/convocations/{id}/sendOne', name: 'api_convocation_send_one', methods: ['POST'])]
    public function sendConvocation(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['id' => 'id'])] Convocation $convocation,
        ConvocationManager $convocationManager
    ): JsonResponse {
        $convocationManager->sendConvocation($convocation);

        return $this->json($convocation, 200, [], ['groups' => ['convocation', 'user', 'party:read']]);
    }
}
