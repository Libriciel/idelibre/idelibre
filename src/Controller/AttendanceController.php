<?php

namespace App\Controller;

use App\Entity\AttendanceToken;
use App\Entity\Convocation;
use App\Entity\Sitting;
use App\Form\AttendanceType;
use App\Service\Convocation\ConvocationAttendance;
use App\Service\Convocation\ConvocationManager;
use App\Service\Email\EmailNotSendException;
use App\Service\role\RoleManager;
use Doctrine\DBAL\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AttendanceController extends AbstractController
{
    public function __construct(
        private readonly ConvocationManager $convocationManager,
        private readonly RoleManager        $roleManager,
    ) {
    }

    #[Route('/attendance/confirmation/{token}', name: 'app_attendance_confirmation')]
    public function confirmAttendanceFromEmail(AttendanceToken $attendanceToken, Request $request): Response
    {
        $roleDeputy = $this->roleManager->getDeputyRole();

        if ($attendanceToken->getConvocation()->getUser()->getRole()->getId() === $roleDeputy->getId()) {
            return $this->redirectToRoute('app_attendance_deputy', ['token' => $attendanceToken->getToken()]);
        }

        $convocation = $attendanceToken->getConvocation();
        $user = $attendanceToken->getConvocation()->getUser();
        $sitting = $attendanceToken->getConvocation()->getSitting();
        $deputyId = $user->getDeputy() ? $user->getDeputy()->getId() : null;
        $toExclude = $this->convocationManager->getAlreadyMandatorBySitting($attendanceToken->getConvocation()->getSitting()->getConvocations()->toArray());
        $deputiesToExclude = $this->convocationManager->getDeputiestoExclude($attendanceToken->getConvocation());

        $previousDeputy = $this->convocationManager->findPreviousDeputyIfAny($attendanceToken->getConvocation());

        $form = $this->createForm(AttendanceType::class, null, [
            'isRemoteAllowed' => $sitting->getIsRemoteAllowed(),
            'isMandatorAllowed' => $sitting->isMandatorAllowed(),
            'convocation' => $convocation,
            'sitting' => $sitting,
            'deputyId' => $deputyId,
            'toExclude' => $toExclude,
            'deputiesToExclude' => $deputiesToExclude
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->get('attendance')->getData() === 'poa' && !$form->get('mandataire')->getData()) {
                $this->addFlash('error', 'Vous devez choisir un mandataire pour donner procuration');

                return $this->redirect($request->headers->get('referer'));
            }

            $convocationAttendance = (new ConvocationAttendance())
                ->setAttendance($form->get('attendance')->getData());

            $sitting->isMandatorAllowed() && $form->get('mandataire')->getData()
                ? $convocationAttendance->setMandataireId($form->get('mandataire')->getData()->getUser()->getId())
                : $convocationAttendance->setMandataireId(null);


            $form->get('deputy')->getData() && $form->get('attendance')->getData() === Convocation::ABSENT_SEND_DEPUTY
                ? $convocationAttendance->setDeputyId($form->get('deputy')->getData()->getId())
                : $convocationAttendance->setDeputyId(null);

            $newDeputy = $form->get('deputy')->getData() ?? null;

            $convocationAttendance->setConvocationId($attendanceToken->getConvocation()->getId());
            $this->convocationManager->updateConvocationAttendances([$convocationAttendance]);
            $this->convocationManager->updateDeputyConvocation($previousDeputy, $newDeputy, $attendanceToken->getConvocation());


            return $this->redirectToRoute('app_attendance_redirect', ['token' => $attendanceToken->getToken()]);
        }


        return $this->render('confirm_attendance/confirm.html.twig', [
            'user' => $user,
            'sitting' => $sitting,
            'deputyId' => $deputyId,
            'convocation' => $convocation,
            'token' => $attendanceToken->getToken(),
            'attendance' => $attendanceToken->getConvocation()->getAttendance(),
            'timezone' => $attendanceToken->getConvocation()->getSitting()->getStructure()->getTimezone()->getName(),
            'form' => $form->createView(),
        ]);
    }

    #[Route('/attendance/redirect/{token}', name: 'app_attendance_redirect')]
    public function attendanceRedirect(AttendanceToken $attendanceToken): Response
    {
        return $this->render('confirm_attendance/attendance_redirect.html.twig', [
            'token' => $attendanceToken->getToken(),
            'attendance' => $attendanceToken->getConvocation()->getAttendance(),
            'convocation' => $attendanceToken->getConvocation(),
        ]);
    }

    #[Route('/attendance/confirmation/deputy/{token}/', name: 'app_attendance_deputy')]
    public function deputyRedirect(AttendanceToken $attendanceToken, Request $request): Response
    {
        return $this->render('confirm_attendance/deputy_redirect.html.twig', [
            'token' => $attendanceToken->getToken(),
            'attendance' => $attendanceToken->getConvocation()->getAttendance(),
            'convocation' => $attendanceToken->getConvocation(),
        ]);
    }


    #[Route('/attendance/availableMandator/{sitting}', name: 'app_available_mandator_convocations')]
    public function getAvailableMandatorConvocations(Sitting $sitting, ConvocationManager $convocationManager): JsonResponse
    {
        if ($sitting->isMandatorAllowed() === false) {
            return new JsonResponse([]);
        }

        $availableMandatorConvocations = $convocationManager->getAvailableMandatorConvocationsBySitting($sitting);

        return $this->json(
            $availableMandatorConvocations,
            200,
            [],
            ['groups' => ['convocation', 'user']]
        );
    }
}
