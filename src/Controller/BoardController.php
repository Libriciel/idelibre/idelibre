<?php

namespace App\Controller;

use App\Entity\Structure;
use App\Service\Seance\SittingManager;
use App\Sidebar\Annotation\Sidebar;
use Knp\Component\Pager\PaginatorInterface;
use Libriciel\Breadcrumbs\Entity\BreadcrumbElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Sidebar(active: ['board-nav'])]
#[BreadcrumbElement('Tableau de bord', 'board_index')]
class BoardController extends AbstractController
{
    #[Route(path: '/board', name: 'board_index')]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    public function index(SittingManager $sittingManager, PaginatorInterface $paginator, Request $request): Response
    {
        /** @var Structure $structure */
        $structure = $this->getUser()->getStructure();
        $sittings = $paginator->paginate(
            $sittingManager->getActiveSittingDetails($this->getUser()),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['s.date'],
                'defaultSortDirection' => 'desc',
            ]
        );

        return $this->render('board/index.html.twig', [
            'sittings' => $sittings,
            'timezone' => $structure->getTimezone()->getName(),
        ]);
    }
}
