<?php

namespace App\Controller\Easy;

use App\Entity\Convocation;
use App\Form\AttendanceType;
use App\Form\GroupStructureType;
use App\Service\Convocation\ConvocationAttendance;
use App\Service\Convocation\ConvocationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class AttendanceController extends AbstractController
{
    #[Route(path: '/attendance/{id}', name: 'easy_attendance')]
    #[IsGranted('ROLE_CONFIRM_ATTENDANCE')]
    public function index(Convocation $convocation, Request $request, ConvocationManager $convocationManager): Response
    {
        $sitting = $convocation->getSitting();
        $toExclude = $convocationManager->getAlreadyMandatorBySitting($sitting->getConvocations()->toArray());
        $deputiesToExclude = $convocationManager->getDeputiestoExclude($convocation);

        $previousDeputy = $convocationManager->findPreviousDeputyIfAny($convocation);


        $form = $this->createForm(AttendanceType::class, null, [
            'isRemoteAllowed' => $sitting->getIsRemoteAllowed(),
            'isMandatorAllowed' => $sitting->isMandatorAllowed(),
            'convocation' => $convocation,
            'sitting' => $sitting,
            'toExclude' => $toExclude,
            'deputyId' => $this->getUser()->getDeputy() ? $this->getUser()->getDeputy()->getId() : null,
            'deputiesToExclude' => $deputiesToExclude
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->get('attendance')->getData() === 'poa' && !$form->get('mandataire')->getData()) {
                $this->addFlash('error', 'Vous devez choisir un mandataire pour donner procuration');

                return $this->redirect($request->headers->get('referer'));
            }

            $convocationAttendance = (new ConvocationAttendance())
                ->setAttendance($form->get('attendance')->getData());

            $convocationAttendance->setMandataireId(null);

            $sitting->isMandatorAllowed() && $form->get('mandataire')->getData()
                ? $convocationAttendance->setMandataireId($form->get('mandataire')->getData()->getUser()->getId())
                : $convocationAttendance->setMandataireId(null);

            $form->get('deputy')->getData() && $form->get('attendance')->getData() === Convocation::ABSENT_SEND_DEPUTY
                ? $convocationAttendance->setDeputyId($form->get('deputy')->getData()->getId())
                : $convocationAttendance->setDeputyId(null);

            $newDeputy = $form->get('deputy')->getData() ?? null;

            $convocationAttendance->setConvocationId($convocation->getId());
            $convocationManager->updateConvocationAttendances([$convocationAttendance]);
            $convocationManager->updateDeputyConvocation($previousDeputy, $newDeputy, $convocation);


            return $this->redirectToRoute('easy_odj_index', ['id' => $convocation->getSitting()->getId()]);
        }

        return $this->render('easy/attendance/index.twig', [
            'user' => $this->getUser(),
            'sitting' => $sitting,
            'deputyId' => $this->getUser()->getDeputy() ? $this->getUser()->getDeputy()->getId() : null,
            'attendance' => $convocation->getAttendance(),
            'convocation' => $convocation,
            'timezone' => $sitting->getStructure()->getTimezone()->getName(),
            'form' => $form->createView()
        ]);
    }
}
