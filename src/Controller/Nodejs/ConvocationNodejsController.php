<?php

namespace App\Controller\Nodejs;

use App\Repository\ConvocationRepository;
use App\Repository\UserRepository;
use App\Security\Http403Exception;
use App\Service\Convocation\ConvocationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ConvocationNodejsController extends AbstractController
{
    #[Route(path: '/node/sendDeputy', name: 'node_send_convocation_deputy', methods: ['POST'])]
    public function sendConvocationDeputy(
        Request               $request,
        ParameterBagInterface $bag,
        ConvocationRepository $convocationRepository,
        UserRepository        $userRepository,
        ConvocationManager    $convocationManager
    ): Response {
        $data = json_decode($request->getContent(), true);

        if ($data['passphrase'] !== $bag->get('nodejs_passphrase')) {
            throw new Http403Exception('Not authorized');
        }

        $user = $userRepository->find($data['userId']);

        if (!$user || !$user->getDeputy()) {
            return $this->json(['success' => false, 'message' => 'User or deputy not found']);
        }

        $convocationDeputy = $convocationRepository->findOneBy(['user' => $user->getDeputy(), 'sitting' => $data['sittingId']]);

        if (!$convocationDeputy) {
            return $this->json(['success' => false, 'message' => 'Convocation deputy not found']);
        }

        $convocationManager->sendConvocation($convocationDeputy);

        return $this->json(['success' => true]);
    }
}
