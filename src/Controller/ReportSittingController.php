<?php

namespace App\Controller;

use App\Entity\Connector\LsvoteConnector;
use App\Entity\Sitting;
use App\Service\Connector\Lsvote\LsvoteException;
use App\Service\Connector\LsvoteConnectorManager;
use App\Service\File\Generator\FileGenerator;
use App\Service\File\Generator\UnsupportedExtensionException;
use App\Service\Report\CsvSittingReport;
use App\Service\Report\PdfSittingReport;
use App\Service\Util\FileUtil;
use App\Service\Zip\ZipTokenGenerator;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\UnavailableStream;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ReportSittingController extends AbstractController
{
    public function __construct(private readonly FileGenerator $fileGenerator)
    {
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     * @throws UnsupportedExtensionException
     */
    #[Route(path: '/reportSitting/pdf/{id}', name: 'sitting_report_pdf')]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function pdfReport(Sitting $sitting, PdfSittingReport $pdfSittingReport, FileUtil $fileUtil): Response
    {

        $directoryPath = $this->fileGenerator->getDirectoryPathByExtension('pdf') . $sitting->getStructure()->getId();
        $finalPath = $directoryPath . '/' . $sitting->getId() . '_report_pdf_sitting.pdf';

        if (file_exists($finalPath)) {
            $response = new BinaryFileResponse($finalPath, 200, ['Content-Type' => 'application/pdf']);
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $fileUtil->sanitizeName($sitting->getName()) . '_report_pdf_sitting.pdf'
            );

            return $response;
        }

        $response = new BinaryFileResponse($pdfSittingReport->generate($sitting), 200, ['Content-Type' => 'application/pdf']);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileUtil->sanitizeName($sitting->getName()) . '_report_pdf_sitting.pdf'
        );
        $response->deleteFileAfterSend();

        return $response;
    }

    /**
     * @throws UnavailableStream
     * @throws CannotInsertRecord
     * @throws Exception
     * @throws UnsupportedExtensionException
     */
    #[Route(path: '/reportSitting/csv/{id}', name: 'sitting_report_csv')]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function csvReport(Sitting $sitting, CsvSittingReport $csvSittingReport, FileUtil $fileUtil): Response
    {
        $directoryPath = $this->fileGenerator->getDirectoryPathByExtension('csv') . $sitting->getStructure()->getId();
        $finalPath = $directoryPath . '/' . $sitting->getId() . '_report_csv_sitting.csv';

        if (file_exists($finalPath)) {
            $response = new BinaryFileResponse($finalPath, 200, ['Content-Type' => 'text/csv']);
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $fileUtil->sanitizeName($sitting->getName()) . '_report_csv_sitting.csv'
            );

            return $response;
        }

        $response = new BinaryFileResponse($csvSittingReport->generate($sitting), 200, ['Content-Type' => 'text/csv']);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileUtil->sanitizeName($sitting->getName()) . '_report_csv_sitting.csv',
        );
        $response->deleteFileAfterSend();

        return $response;
    }

    #[Route(path: '/reportSitting/token/{id}', name: 'sitting_report_token')]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function getSittingZipTokens(Sitting $sitting, ZipTokenGenerator $zipTokenGenerator, FileUtil $fileUtil): Response
    {
        $response = new BinaryFileResponse($zipTokenGenerator->generateZipToken($sitting));
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileUtil->sanitizeName($sitting->getName()) . '_' . $sitting->getDate()->format('d_m_Y_H_i') . '_jetons.zip'
        );
        $response->deleteFileAfterSend();

        return $response;
    }
}
