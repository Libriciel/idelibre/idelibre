<?php

declare(strict_types=1);

namespace App\Controller\Sitting;

use App\Entity\Sitting;
use App\Repository\ConvocationRepository;
use App\Repository\EmailTemplateRepository;
use App\Repository\UserRepository;
use App\Service\Connector\LsvoteConnectorManager;
use App\Service\Convocation\ConvocationManager;
use App\Service\EmailTemplate\EmailGenerator;
use App\Sidebar\Annotation\Sidebar;
use App\Sidebar\State\SidebarState;
use Exception;
use Libriciel\Breadcrumbs\Entity\BreadcrumbElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class SittingActorsController extends AbstractController
{

    public function __construct(
        private readonly ConvocationManager $convocationManager,
        private readonly SidebarState $sidebarState,
        private readonly LsvoteConnectorManager $lsvoteConnectorManager,
        private readonly ConvocationRepository $convocationRepository,
    ) {
    }


    /**
     * @throws Exception
     */
    #[Route(path: '/sitting/edit/{id}/actors', name: 'edit_sitting_actor', methods: ['GET'])]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    #[Sidebar(active: ['sitting-active-nav'])]
    #[BreadcrumbElement('Modification des destinataires de la séance {sitting.nameWithDate}')]
    public function editUsers(Sitting $sitting): Response
    {
        if ($sitting->getIsArchived()) {
            throw new InvalidArgumentException('Impossible de modifier une séance classée');
        }

        return $this->render('sitting/edit_actors.html.twig', [
            'sitting' => $sitting,
            'title' => 'Modification des destinataires de la séance ' . $sitting->getNameWithDate(),
        ]);
    }

    #[Route(path: '/sitting/show/{id}/actors', name: 'sitting_show_actors', methods: ['GET'])]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    #[BreadcrumbElement('Détail {sitting.nameWithDate}')]
    public function showActors(Sitting $sitting, EmailTemplateRepository $emailTemplateRepository, EmailGenerator $emailGenerator): Response
    {
        $this->sidebarState->setActiveNavs(['sitting-nav', $this->activeSidebarNav($sitting->getIsArchived())]);

        $emailTemplate = $emailTemplateRepository->findOneByStructureAndCategory($sitting->getStructure(), 'convocation');
        $emailTemplateBySittingType = $emailTemplateRepository->findOneByStructureAndCategoryAndType($sitting->getStructure(), $sitting->getType(), 'convocation');
        $emailTemplateInvitation = $emailTemplateRepository->findOneByStructureAndCategory($sitting->getStructure(), 'invitation');

        $subjectGenerated = $emailGenerator->generateEmailTemplateSubject($sitting, $emailTemplate->getSubject());
        $subjectInvitation = $emailGenerator->generateEmailTemplateSubject($sitting, $emailTemplateInvitation->getSubject());

        $subjectBySittingTypeGenerated = '';
        if (isset($emailTemplateBySittingType)) {
            $subjectBySittingTypeGenerated = $emailGenerator->generateEmailTemplateSubject($sitting, $emailTemplateBySittingType->getSubject());
        }

        return $this->render('sitting/details_actors.html.twig', [
            'sitting' => $sitting,
            'emailTemplate' => $emailTemplate,
            'emailTemplateBySittingType' => $emailTemplateBySittingType,
            'emailTemplateInvitation' => $emailTemplateInvitation,
            'subjectGenerated' => $subjectGenerated,
            'subjectBySittingTypeGenerated' => $subjectBySittingTypeGenerated,
            'subjectInvitation' => $subjectInvitation,
            'isActiveLsvote' => $this->lsvoteConnectorManager->getLsvoteConnector($sitting->getStructure())->getActive(),
            'convocationNotAnswered' => $this->convocationManager->countConvocationNotanswered($sitting->getConvocations()),
            'countGuest' => count($this->convocationRepository->getGuestConvocationsBySitting($sitting)),
            'countEmployee' => count($this->convocationRepository->getInvitableEmployeeConvocationsBySitting($sitting)),
        ]);
    }

    #[Route('/sitting/{id}/list/actors', name: 'sitting_actors_list', methods: ['GET'])]
    public function getAllActorsList(UserRepository $userRepository): Response
    {
        return $this->render('sitting/includes/_list_actors.html.twig', [
            "actors" => $userRepository->findActorsInStructure($this->getUser()->getStructure())->getQuery()->getResult(),
        ]);
    }

    private function activeSidebarNav(bool $isArchived): string
    {
        if ($isArchived) {
            return 'sitting-archived-nav';
        }

        return 'sitting-active-nav';
    }

}
