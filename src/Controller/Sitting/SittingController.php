<?php

namespace App\Controller\Sitting;

use App\Entity\Sitting;
use App\Form\SearchType;
use App\Service\File\Generator\FileGenerator;
use App\Service\File\Generator\UnsupportedExtensionException;
use App\Service\Seance\SittingManager;
use App\Sidebar\Annotation\Sidebar;
use App\Sidebar\State\SidebarState;
use Knp\Component\Pager\PaginatorInterface;
use Libriciel\Breadcrumbs\Entity\BreadcrumbElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Sidebar(active: ['sitting-nav'])]
#[BreadcrumbElement('Séances', 'sitting_index')]
class SittingController extends AbstractController
{
    public function __construct(
        private readonly SittingManager $sittingManager,
        private readonly SidebarState $sidebarState,
        private readonly FileGenerator $fileGenerator,
    ) {
    }

    #[Route(path: '/sitting', name: 'sitting_index')]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    public function index(
        PaginatorInterface $paginator,
        Request $request,
    ): Response
    {
        $formSearch = $this->createForm(SearchType::class);
        $sittings = $paginator->paginate(
            $this->sittingManager->getListSittingByStructureQuery($this->getUser(), $request->query->get('search'), $request->query->get('status')),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['s.date'],
                'defaultSortDirection' => 'desc',
            ]
        );
        if ($status = $request->query->get('status')) {
            $this->sidebarState->setActiveNavs(['sitting-nav', "sitting-$status-nav"]);
        }

        return $this->render('sitting/index.html.twig', [
            'sittings' => $sittings,
            'formSearch' => $formSearch->createView(),
            'searchTerm' => $request->query->get('search') ?? '',
            'timezone' => $this->getUser()->getStructure()->getTimezone()->getName(),
        ]);
    }


    /**
     * @throws UnsupportedExtensionException
     */
    #[Route(path: '/sitting/delete/{id}', name: 'sitting_delete', methods: ['DELETE'])]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function delete(Sitting $sitting, Request $request): Response
    {
        $this->sittingManager->delete($sitting);
        $this->addFlash('success', 'La séance a bien été supprimée');
        $referer = $request->headers->get('referer');

        return $referer ? $this->redirect($referer) : $this->redirectToRoute('sitting_index');
    }

    /**
     * @throws UnsupportedExtensionException
     */
    #[Route(path: '/sitting/zip/{id}', name: 'sitting_zip', methods: ['GET'])]
    #[IsGranted('DOWNLOAD_ZIP', subject: 'sitting')]
    public function getZipSitting(Sitting $sitting): Response
    {
        $zipPath = $this->fileGenerator->genFullSittingDirPath($sitting, 'zip');
        $response = new BinaryFileResponse($zipPath);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->fileGenerator->createPrettyName($sitting, 'zip')
        );
        $response->headers->set('X-Accel-Redirect', $zipPath);

        return $response;
    }

    /**
     * @throws UnsupportedExtensionException
     */
    #[Route(path: '/sitting/pdf/{id}', name: 'sitting_full_pdf', methods: ['GET'])]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function getFullPdfSitting(Sitting $sitting): Response
    {
        $pdfPath = $this->fileGenerator->genFullSittingDirPath($sitting, 'pdf');
        $response = new BinaryFileResponse($pdfPath);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->fileGenerator->createPrettyName($sitting, 'pdf')
        );
        $response->headers->set('X-Accel-Redirect', $pdfPath);

        return $response;
    }

    #[Route(path: '/sitting/archive/{id}', name: 'sitting_archive', methods: ['POST'])]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function archiveSitting(Sitting $sitting, Request $request): Response
    {
        $this->sittingManager->archive($sitting);

        $this->addFlash('success', 'La séance a été classée');
        $referer = $request->headers->get('referer');

        return $referer ? $this->redirect($referer) : $this->redirectToRoute('sitting_index');
    }

    #[Route(path: '/sitting/unarchive/{id}', name: 'sitting_unarchive', methods: ['POST'])]
    #[IsGranted('ROLE_SUPERADMIN')]
    public function unArchiveSitting(Sitting $sitting, Request $request): Response
    {
        $this->sittingManager->unArchive($sitting);
        $this->addFlash('success', 'La séance a été déclassée');
        $referer = $request->headers->get('referer');

        return $referer ? $this->redirect($referer) : $this->redirectToRoute('sitting_index');
    }

}
