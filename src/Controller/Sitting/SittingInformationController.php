<?php

declare(strict_types=1);

namespace App\Controller\Sitting;

use App\Entity\Sitting;
use App\Form\SittingType;
use App\Service\Connector\LsvoteConnectorManager;
use App\Service\Pdf\PdfValidator;
use App\Service\Seance\SittingManager;
use App\Sidebar\Annotation\Sidebar;
use App\Sidebar\State\SidebarState;
use Exception;
use Libriciel\Breadcrumbs\Entity\BreadcrumbElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class SittingInformationController extends AbstractController
{

    public function __construct(
        private readonly SittingManager $sittingManager,
        private readonly SidebarState $sidebarState,
        private readonly LsvoteConnectorManager $lsvoteConnectorManager,
        private readonly PdfValidator $pdfValidator,
    ) {
    }
    #[Route(path: '/sitting/add', name: 'sitting_add')]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    #[Sidebar(active: ['sitting-active-nav'])]
    #[BreadcrumbElement('Ajouter une séance')]
    public function createSitting(Request $request): Response
    {
        $form = $this->createForm(SittingType::class, null, ['structure' => $this->getUser()->getStructure(), 'user' => $this->getUser()]);
        $form->handleRequest($request);

        $unreadablePdf = $this->pdfValidator->getListOfUnreadablePdf([
            $form->get('convocationFile')->getData(),
            $form->get('invitationFile')->getData(),
        ]);

        if (count($unreadablePdf) > 0) {
            $this->addFlash('error', 'Fichier(s) invalide(s) :  ' . implode(', ', $unreadablePdf));

            return $this->redirectToRoute('sitting_add');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $sittingId = $this->sittingManager->save(
                $form->getData(),
                $form->get('convocationFile')->getData(),
                $form->get('invitationFile')->getData(),
                $this->getUser()->getStructure(),
                $form->get('reminder')->getData()
            );

            return $this->redirectToRoute('edit_sitting_actor', ['id' => $sittingId]);
        }

        return $this->render('sitting/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @throws Exception
     */
    #[Route(path: '/sitting/edit/{id}', name: 'edit_sitting_information')]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    #[Sidebar(active: ['sitting-active-nav'])]
    #[BreadcrumbElement('Modification des informations de la séance {sitting.nameWithDate}')]
    public function editInformation(Sitting $sitting, Request $request): Response
    {
        if ($sitting->getIsArchived()) {
            throw new InvalidArgumentException('Impossible de modifier une séance classée');
        }
        $form = $this->createForm(SittingType::class, $sitting, [
            'structure' => $this->getUser()->getStructure(),
        ]);
        $form->handleRequest($request);

        $unreadablePdf = $this->pdfValidator->getListOfUnreadablePdf([
            $form->get('convocationFile')->getData(),
            $form->get('invitationFile')->getData(),
        ]);

        if (count($unreadablePdf) > 0) {
            $this->addFlash('error', 'Fichier(s) invalide(s) :  ' . implode(', ', $unreadablePdf));

            return $this->redirectToRoute('sitting_add');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->sittingManager->update(
                $form->getData(),
                $form->get('convocationFile')->getData(),
                $form->get('invitationFile')->getData(),
            );

            $this->addFlash('success', 'Modifications enregistrées');

            return $this->redirectToRoute('sitting_show_information', ['id' => $sitting->getId()]);
        }

        return $this->render('sitting/edit_information.html.twig', [
            'form' => $form->createView(),
            'sitting' => $sitting,
            'title' => 'Modification des informations de la séance ' . $sitting->getNameWithDate(),
        ]);
    }


    #[Route(path: '/sitting/show/{id}/information', name: 'sitting_show_information', methods: ['GET'])]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    #[BreadcrumbElement('Détail {sitting.nameWithDate}')]
    public function showInformation(Sitting $sitting, ParameterBagInterface $bag): Response
    {
        $this->sidebarState->setActiveNavs(['sitting-nav', $this->activeSidebarNav($sitting->getIsArchived())]);

        return $this->render('sitting/details_information.html.twig', [
            'isAlreadySent' => $this->sittingManager->isAlreadySent($sitting),
            'sitting' => $sitting,
            'isActiveLsvote' => $this->lsvoteConnectorManager->getLsvoteConnector($sitting->getStructure())->getActive(),
            'isLsvoteResults' => !empty($sitting->getLsvoteSitting()?->getResults()),
            'isSentLsvote' => !empty($sitting->getLsvoteSitting()),
            'timezone' => $sitting->getStructure()->getTimezone()->getName(),
            'isTotalSizeTooBig' => $this->sittingManager->getAllFilesSize($sitting) > intval($bag->get('maximum_size_pdf_zip_generation')),
            'totalFilesSize' => $this->sittingManager->getAllFilesSize($sitting),
        ]);
    }

    #[Route(path: '/sitting/edit/{id}/cancel', name: 'edit_sitting_information_cancel')]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function editInformationCancel(Sitting $sitting): Response
    {
        $this->addFlash('success', 'Modifications annulées');

        return $this->redirectToRoute('edit_sitting_information', ['id' => $sitting->getId()]);
    }

    #[Route('sitting/{id}/information/removeInvitation')]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function removeInvitationFile(Sitting $sitting): JsonResponse
    {
        if ($this->sittingManager->isAlreadySent($sitting)) {
            throw new BadRequestException();
        }
        $this->sittingManager->removeInvitationFile($sitting);
        $this->sittingManager->deleteInvitations($sitting);

        return $this->json(['success' => true]);
    }

    private function activeSidebarNav(bool $isArchived): string
    {
        if ($isArchived) {
            return 'sitting-archived-nav';
        }

        return 'sitting-active-nav';
    }
}
