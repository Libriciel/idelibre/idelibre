<?php

declare(strict_types=1);

namespace App\Controller\Sitting;

use App\Entity\Sitting;
use App\Repository\OtherdocRepository;
use App\Repository\ProjectRepository;
use App\Service\Seance\SittingManager;
use App\Sidebar\Annotation\Sidebar;
use App\Sidebar\State\SidebarState;
use Exception;
use Libriciel\Breadcrumbs\Entity\BreadcrumbElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class SittingProjectController extends AbstractController
{

    public function __construct(
        private readonly SittingManager $sittingManager,
        private readonly SidebarState $sidebarState,
    ) {
    }

    #[Route(path: '/sitting/show/{id}/projects', name: 'sitting_show_projects', methods: ['GET'])]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    #[BreadcrumbElement('Détail {sitting.nameWithDate}')]
    public function showProjects(Sitting $sitting, ProjectRepository $projectRepository, OtherdocRepository $otherdocRepository, ParameterBagInterface $bag): Response
    {
        $this->sidebarState->setActiveNavs(['sitting-nav', $this->activeSidebarNav($sitting->getIsArchived())]);

        return $this->render('sitting/details_projects.html.twig', [
            'sitting' => $sitting,
            'projects' => $projectRepository->getProjectsWithAssociatedEntities($sitting),
            'otherdocs' => $otherdocRepository->getOtherdocsWithAssociatedEntities($sitting),
            'projectsFilesSize' => $this->sittingManager->getProjectsAndAnnexesTotalSize($sitting),
            'otherdocsFilesSize' => $this->sittingManager->getOtherDocsTotalSize($sitting),
            'totalFilesSize' => $this->sittingManager->getAllFilesSize($sitting),
            'isProjectsSizeTooBig' => $this->sittingManager->getProjectsAndAnnexesTotalSize($sitting) > intval($bag->get('maximum_size_pdf_zip_generation')),
            'isOthersSizeTooBig' => $this->sittingManager->getOtherDocsTotalSize($sitting) > intval($bag->get('maximum_size_pdf_zip_generation')),
            'isTotalSizeTooBig' => $this->sittingManager->getAllFilesSize($sitting) > intval($bag->get('maximum_size_pdf_zip_generation')),
        ]);
    }


    /**
     * @throws Exception
     */
    #[Route(path: '/sitting/edit/{id}/projects', name: 'edit_sitting_project')]
    #[Sidebar(active: ['sitting-active-nav'])]
    #[BreadcrumbElement('Modification des projets de la séance {sitting.nameWithDate}')]
    public function editProjects(Sitting $sitting): Response
    {
        if ($sitting->getIsArchived()) {
            throw new InvalidArgumentException('Impossible de modifier une séance classé');
        }

        return $this->render('sitting/edit_projects.html.twig', [
            'sitting' => $sitting,
            'title' => 'Modification des projets de la séance ' . $sitting->getNameWithDate(),
        ]);
    }

    private function activeSidebarNav(bool $isArchived): string
    {
        if ($isArchived) {
            return 'sitting-archived-nav';
        }

        return 'sitting-active-nav';
    }


}
