<?php

declare(strict_types=1);

namespace App\Controller\Sitting;

use App\Entity\Sitting;
use App\Repository\LsvoteConnectorRepository;
use App\Service\Connector\Lsvote\LsvoteException;
use App\Service\Connector\LsvoteConnectorManager;
use App\Service\Connector\LsvoteResultException;
use App\Service\File\Generator\FileGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class SittingVotesResultsController extends AbstractController
{
    public function __construct(
        private readonly FileGenerator $fileGenerator,
        private readonly LsvoteConnectorManager $lsvoteConnectorManager,
    ) {
    }

    #[Route(path: '/sitting/{id}/lsvote-results', name: 'sitting_lsvote_results', methods: ['GET'])]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    public function getLsvoteResults(Sitting $sitting, Request $request): Response
    {
        try {
            $this->lsvoteConnectorManager->getLsvoteSittingResults($sitting);
        } catch (LsvoteResultException $e) {
            $this->addFlash('error', $e->getMessage());

            return $this->redirect($request->headers->get('referer'));
        }

        $this->addFlash('success', 'Les résultats ont bien été récupérés depuis lsvote');

        return $this->redirect($request->headers->get('referer'));
    }



    #[Route(path: '/sitting/{id}/lsvote-results/json', name: 'sitting_lsvote_results_json', methods: ['GET'])]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    public function downloadLsvoteResultsCsv(Sitting $sitting): Response
    {
        $jsonPath = $this->lsvoteConnectorManager->createJsonFile($sitting);

        $response = new BinaryFileResponse($jsonPath);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'lsvote_results_' . $this->fileGenerator->createPrettyName($sitting, "json")
        );

        $response->deleteFileAfterSend();

        return $response;
    }

    /**
     * @throws LsvoteException
     */
    #[Route(path: '/sitting/{id}/lsvote-results/pdf', name: 'sitting_lsvote_results_pdf', methods: ['GET'])]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    public function fetchLsvoteResultPdf(Sitting $sitting, LsvoteConnectorRepository $lsvoteConnectorRepository): Response
    {
        $lsvoteConnector = $lsvoteConnectorRepository->findOneBy(["structure" => $sitting->getStructure()]);
        $pdfPath = $this->lsvoteConnectorManager->fetchLsvoteResultsPdf($lsvoteConnector, $sitting);

        $response = new BinaryFileResponse($pdfPath);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'lsvote_results_' . $this->fileGenerator->createPrettyName($sitting, "pdf")
        );
        $response->deleteFileAfterSend();

        return $response;
    }
}
