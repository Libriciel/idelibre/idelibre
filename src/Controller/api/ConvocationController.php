<?php

namespace App\Controller\api;

use App\Entity\Convocation;
use App\Entity\Sitting;
use App\Repository\ConvocationRepository;
use App\Repository\UserRepository;
use App\Service\Convocation\ConvocationAttendance;
use App\Service\Convocation\ConvocationAttendanceManager;
use App\Service\Convocation\ConvocationManager;
use App\Service\Email\EmailData;
use App\Service\EmailTemplate\EmailGenerator;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ConvocationController extends AbstractController
{
    public function __construct(
        private readonly ConvocationManager           $convocationManager,
        private readonly UserRepository               $userRepository,
        private readonly ConvocationRepository        $convocationRepository,
        private readonly ConvocationAttendanceManager $convocationAttendanceManager,
        private readonly DenormalizerInterface $denormalizer
    ) {
    }

    #[Route(path: '/api/convocations/{id}', name: 'api_convocation_sitting', methods: ['GET'])]
    #[IsGranted('MANAGE_SITTINGS', subject: 'sitting')]
    public function getConvocations(Sitting $sitting, ConvocationRepository $convocationRepository, ConvocationManager $convocationManager): JsonResponse
    {
        return $this->json(
            [
                'actors' => $convocationRepository->getActorConvocationsBySitting($sitting),
                'guests' => $convocationRepository->getGuestConvocationsBySitting($sitting),
                'employees' => $convocationRepository->getInvitableEmployeeConvocationsBySitting($sitting),
                'deputies' => $convocationRepository->getDeputyConvocationsBySitting($sitting) ?? [],

                'quorum' => $convocationManager->getQuorum($sitting),
                'inTheRoom' => $convocationManager->getInTheRoom($sitting),
                'poa' => $convocationManager->getPoa($sitting),
                'assignedAndPresentDeputies' => $convocationManager->getAssignedandPresentDeputy($sitting),

            ],
            200,
            [],
            ['groups' => ['convocation', 'user', 'userAssociated', 'party:read', 'user:titular']]
        );
    }

    #[Route(path: '/api/convocations/{id}/send', name: 'api_convocation_send', methods: ['POST'])]
    #[IsGranted('MANAGE_CONVOCATIONS', subject: 'convocation')]
    public function sendConvocation(Convocation $convocation, ConvocationManager $convocationManager): JsonResponse
    {
        $convocationManager->sendConvocation($convocation);

        return $this->json($convocation, 200, [], ['groups' => ['convocation', 'user', 'party:read']]);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route(path: '/api/convocations/attendance', name: 'api_convocation_attendance', methods: ['POST', 'PUT'])]
    #[IsGranted('MANAGE_ATTENDANCE', subject: 'request')]
    public function setAttendance(ConvocationManager $convocationManager, Request $request, DenormalizerInterface $denormalizer): JsonResponse
    {
        $convocationAttendances = $denormalizer->denormalize($request->toArray(), ConvocationAttendance::class . '[]', context: ['normalize_relations' => true]);
        $sitting = $this->convocationRepository->findOneBy(["id" => $convocationAttendances[0]->getConvocationId()])->getSitting();

        $this->convocationAttendanceManager->updateAttendanceForDeputies($convocationAttendances, $sitting);
        $convocationManager->validateConvocationAttendancesMandator($convocationAttendances);
        $convocationManager->updateConvocationAttendances($convocationAttendances);

        return $this->json(['success' => 'true']);
    }

    #[Route(path: '/api/convocations/previewForSecretary/{id}', name: 'api_convocation_preview_for_secretary', methods: ['GET'])]
    #[IsGranted('MANAGE_CONVOCATIONS', subject: 'convocation')]
    public function iframePreviewForSecretary(Convocation $convocation, EmailGenerator $generator): Response
    {
        $convocation->setCategory('convocation');
        $emailData = $generator->generateFromTemplateAndConvocation($convocation->getSitting()->getType()->getEmailTemplate(), $convocation);
        $content = $emailData->getContent();
        if (EmailData::FORMAT_TEXT === $emailData->getFormat()) {
            $content = htmlspecialchars($content);
            $content = nl2br($content);
        }

        return new Response($content);
    }

    #[Route(path: '/api/convocations/previewForSecretaryOther/{id}', name: 'api_convocation_preview_for_secretary_other', methods: ['GET'])]
    #[IsGranted('MANAGE_CONVOCATIONS', subject: 'convocation')]
    public function iframePreviewForSecretaryOther(Convocation $convocation, EmailGenerator $generator): Response
    {
        $convocation->setCategory('invitation');
        $emailData = $generator->generateFromTemplateAndConvocation($convocation->getSitting()->getType()->getEmailTemplate(), $convocation);
        $content = $emailData->getContent();
        if (EmailData::FORMAT_TEXT === $emailData->getFormat()) {
            $content = htmlspecialchars($content);
            $content = nl2br($content);
        }

        return new Response($content);
    }


    #[Route(path: '/api/convocations/previewForDeputy/{id}', name: 'api_convocation_preview_for_deputy', methods: ['GET'])]
    #[IsGranted('MANAGE_CONVOCATIONS', subject: 'convocation')]
    public function iframePreviewForDeputy(Convocation $convocation, EmailGenerator $generator): Response
    {
        $convocation->setCategory(Convocation::CATEGORY_DELEGATION);
        $emailData = $generator->generateFromTemplateAndConvocation($convocation->getSitting()->getType()->getEmailTemplate(), $convocation);
        $content = $emailData->getContent();
        if (EmailData::FORMAT_TEXT === $emailData->getFormat()) {
            $content = htmlspecialchars($content);
            $content = nl2br($content);
        }

        return new Response($content);
    }

    #[Route(path: '/api/convocation/{id}/deputiesAvailable', name: 'api_convocation_deputies_available', methods: ['GET'])]
    #[IsGranted('MANAGE_CONVOCATIONS', subject: 'convocation')]
    public function getDeputiesAvailable(Convocation $convocation, ConvocationManager $convocationManager): JsonResponse
    {
        $deputiesToExclude = $convocationManager->getDeputiestoExclude($convocation);
        $availableDeputies = $this->userRepository->findDeputiesWithNoAssociation($convocation->getSitting()->getStructure(), $deputiesToExclude)->getQuery()->getResult();

        return $this->json(
            [
                'deputiesAvailable' => $availableDeputies,
            ],
            200,
            [],
            ['groups' => ['convocation', 'user']]
        );
    }

    #[Route(path: '/api/convocation/{id}/resetDeputyAttendance', name: 'api_convocation_reset_deputies_attendance', methods: ['GET'])]
    #[IsGranted('MANAGE_CONVOCATIONS', subject: 'convocation')]
    public function resetDeputyAttendance(Convocation $convocation): JsonResponse
    {
        $deputyConvocation = $this->convocationRepository->findOneBy(["id" => $convocation->getId()]);
        $this->convocationAttendanceManager->resetDeputiesAttendanceToAbsent($deputyConvocation);

        return $this->json(['success' => 'true']);
    }
}
