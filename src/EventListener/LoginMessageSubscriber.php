<?php

namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LoginMessageSubscriber implements EventSubscriberInterface
{
    public function onKernelResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $route = $request->attributes->get('_route');

        if ($request->getHost() !== 'idelibre-api.fr') {
            return;
        }

        if ($route === 'app_login') {
            $response = $event->getResponse();
            $content = $response->getContent();

            $message = '
            <div style="position: absolute; top: 50px; left: 50%; transform: translateX(-50%); 
                        background-color: #f8d7da; padding: 10px 20px; 
                        border: 1px solid #f5c6cb; color: #721c24; font-weight: bold;">
                L\'adresse d\'idelibre a changé il faut maintenant se connecter à l\'url <a href="https://idelibre.libriciel.fr"> idelibre.libriciel.fr </a> 
            </div>';

            $newContent = str_replace('</body>', $message . '</body>', $content);

            $response->setContent($newContent);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }
}
