<?php

namespace App\Form;

use App\Entity\Convocation;
use App\Entity\User;
use App\Repository\ConvocationRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttendanceType extends AbstractType
{
    public function __construct(
        private readonly UserRepository        $userRepository,
        private readonly ConvocationRepository $convocationRepository
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $sitting = $options['sitting'];
        $user = $options['convocation']->getUser();

        $builder
            ->add('attendance', ChoiceType::class, [
                'required' => true,
                'label' => 'Merci de confirmer votre présence',
                'row_attr' => ["id" => "attendanceGroup"],
                'choices' => $this->presenceValues($options['convocation']),
                'empty_data' => Convocation::PRESENT,
            ]);

        if ($options['isMandatorAllowed']) {
            $builder
                ->add('mandataire', EntityType::class, [
                    'label' => 'Mandataire',
                    'row_attr' => ["id" => "attendance_mandataire_group", "class" => 'd-none'],
                    'required' => true,
                    'class' => User::class,
                    'query_builder' => $this->convocationRepository->findAvailableMandatorsInSitting($sitting, $user, $options['toExclude']),
                    'choice_label' => fn (Convocation $convocation) => $this->formatName($convocation->getUser()),
                    'choice_value' => fn (?Convocation $convocation) => $convocation?->getUser()->getId(),
                    'disabled' => false,
                ]);
        }


        $builder->add('deputy', EntityType::class, [
            'label' => 'Suppléant',
            'row_attr' => ["id" => "attendance_deputy_group", "class" => 'd-none'],
            'required' => true,
            'class' => User::class,
            'query_builder' => $this->userRepository->findDeputiesWithNoAssociation($sitting->getStructure(), $options['deputiesToExclude']),
            'choice_label' => fn (User $user) => $this->formatName($user),
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'isRemoteAllowed' => false,
            'isMandatorAllowed' => true,
            'convocation' => null,
            'sitting' => null,
            'deputyId' => null,
            'toExclude' => null,
            'deputiesToExclude' => null,
        ]);
    }

    private function formatName(User $user): string
    {
        if ($user->getTitular()) {
            return $user->getLastName() . ' ' . $user->getFirstName() . ' (assigné par défaut)';
        }
        return $user->getLastName() . ' ' . $user->getFirstName();
    }

    private function presenceValues($convocation): array
    {
        $presenceStatusList = [
            'Présent' => Convocation::PRESENT,
            'Absent' => Convocation::ABSENT,
        ];

        if ($convocation->getSitting()->getIsRemoteAllowed() === true) {
            $presenceStatusList = [...$presenceStatusList, ...['Présent à distance' => Convocation::REMOTE]];
        }

        if ($convocation->getCategory() === Convocation::CATEGORY_CONVOCATION) {
            $presenceStatusList = [...$presenceStatusList, ...['Remplacé par un suppléant' => Convocation::ABSENT_SEND_DEPUTY]];
        }

        if ($convocation->getCategory() === Convocation::CATEGORY_CONVOCATION) {
            if ($convocation->getSitting()->isMandatorAllowed() === true) {
                $presenceStatusList = [...$presenceStatusList, ...['Donne pouvoir via procuration' => Convocation::ABSENT_GIVE_POA], ];
            }
        }

        return $presenceStatusList;
    }
}
