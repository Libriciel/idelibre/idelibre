<?php

namespace App\Form\Connector;

use App\Entity\Connector\LsmessageConnector;
use App\Form\Type\LsChoiceType;
use Libriciel\LsMessageWrapper\Sms;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class LsmessageConnectorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $minSenderLength = LsmessageConnector::MIN_SENDER_LENGTH;
        $maxSenderLength = LsmessageConnector::MAX_SENDER_LENGTH;
        $maxContentLength = LsmessageConnector::MAX_CONTENT_LENGTH;

        $builder
            ->add('url', UrlType::class, [
                'required' => true,
                'label' => 'Url',
                'constraints' => [new Length(['max' => Sms::MAX_CHARACTERS_MESSAGE])],
            ])
            ->add('apiKey', TextType::class, [
                'required' => true,
                'label' => 'Clé d\'api',
                'constraints' => [new Length(['max' => LsmessageConnector::MAX_API_KEY_LENGTH])],
            ])
            ->add('sender', TextType::class, [
                'required' => true,
                'label' => 'Expéditeur (minimum ' . $minSenderLength . ' caractères et maximum ' . $maxSenderLength . ' caractères, sans caractères spéciaux ni espaces).',
                'constraints' => [
                    new Length([
                        'min' => LsmessageConnector::MIN_SENDER_LENGTH,
                        'max' => LsmessageConnector::MAX_SENDER_LENGTH,
                        'minMessage' => "L'expéditeur doit contenir au moins $minSenderLength  caractères",
                        'maxMessage' => "L'expéditeur doit contenir au maximum $maxSenderLength  caractères",

                    ]),
                    new Regex('/^[a-zA-Z0-9]+$/', 'L\'expéditeur ne doit ni commencer par un chiffre ni contenir de caractères spéciaux'),
                ],
                'attr' => [
                    'class' => 'sms_count_sender',
                    'onkeyup' => "countCharactersSender(this.value, $maxSenderLength);",
                ],
                'help' => '0 / ' . $maxSenderLength . ' caractères',
            ])
            ->add('content', TextareaType::class, [
                'required' => true,
                'label' => 'Message (maximum ' . $maxContentLength . ' caractères)',
                'attr' => [
                    'rows' => 3,
                    'class' => 'sms_count_message',
                    'onkeyup' => "countCharactersMessage(this.value, $maxContentLength );",
                    'data-nb-max-characters' => $maxContentLength,
                ],
                'help' => '0 / ' . $maxContentLength . ' caractères',
                'constraints' => [new Length(['max' => $maxContentLength])],
            ])
            ->add('active', LsChoiceType::class, [
                'label' => 'Actif',
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'sanitize_html' => true,
            'data_class' => LsmessageConnector::class,
        ]);
    }
}
