<?php

namespace App\Form;

use App\Entity\Reminder;
use App\Entity\Sitting;
use App\Form\Type\LsChoiceType;
use App\Repository\SittingRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReminderSittingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('isActive', LsChoiceType::class, [
                'label' => 'Ajouter au calendrier',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'empty_data' => false
        ])
        ->add('duration', ChoiceType::class, [
            'label' => 'Durée',
            'choices' => Reminder::VALUES,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reminder::class,
            'sitting' => Sitting::class,
        ]);
    }
}
