<?php

namespace App\Repository;

use App\Entity\Convocation;
use App\Entity\Enum\RoleName;
use App\Entity\Role;
use App\Entity\Sitting;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Convocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Convocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Convocation[]    findAll()
 * @method Convocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConvocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Convocation::class);
    }

    /**
     * @return Convocation[]
     */
    public function getActorConvocationsBySitting(Sitting $sitting): array
    {
        return $this->getWithRolesConvocationsBySitting($sitting, [RoleName::NAME_ROLE_ACTOR]);
    }

    /**
     * @return Convocation[]
     */
    public function getInvitableEmployeeConvocationsBySitting(Sitting $sitting): array
    {
        return $this->getWithRolesConvocationsBySitting($sitting, RoleName::INVITABLE_EMPLOYEE);
    }


    public function getDeputyConvocationsBySitting(Sitting $sitting): array
    {
        return $this->getWithRolesConvocationsBySitting($sitting, [RoleName::NAME_ROLE_DEPUTY]);
    }


    /**
     * @return Convocation[]
     */
    public function getGuestConvocationsBySitting(Sitting $sitting): array
    {
        return $this->getWithRolesConvocationsBySitting($sitting, [RoleName::NAME_ROLE_GUEST]);
    }


    public function getEveryOneButDeputyConvocationsBySitting(Sitting $sitting): array
    {
        return $this->getWithRolesConvocationsBySitting($sitting, [RoleName::NAME_ROLE_ACTOR, ...RoleName::INVITABLE_EMPLOYEE, RoleName::NAME_ROLE_GUEST]);
    }


    /**
     * @return Convocation[]
     */
    private function getWithRolesConvocationsBySitting(Sitting $sitting, array $roleNames): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.sitting = :sitting')
            ->setParameter('sitting', $sitting)
            ->leftJoin('c.user', 'user')
            ->addSelect('user')
            ->leftJoin('user.party', 'party')
            ->addSelect('party')
            ->leftJoin('c.deputy', 'deputy')
            ->addSelect('deputy')
            ->innerJoin('user.role', 'r')
            ->andWhere('r.name in (:roleNames)')
            ->setParameter('roleNames', $roleNames)
            ->orderBy('user.lastName')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Convocation[]
     */
    public function getConvocationsBySittingAndActorIds(Sitting $sitting, array $userIds): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.sitting = :sitting')
            ->setParameter('sitting', $sitting)
            ->join('c.user', 'user')
            ->andWhere('user.id in (:userIds)')
            ->setParameter('userIds', $userIds)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string[] $convocationIds
     */
    public function getConvocationsWithUser(array $convocationIds): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id in (:convocationIds)')
            ->setParameter('convocationIds', $convocationIds)
            ->join('c.user', 'user')
            ->addSelect('user')
            ->orderBy('user.lastName')
            ->addOrderBy('user.firstName')
            ->getQuery()
            ->getResult();
    }

    public function getConvocationsWithUserBySitting(Sitting $sitting): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.sitting = :sitting')
            ->setParameter('sitting', $sitting)
            ->join('c.user', 'user')
            ->addSelect('user')
            ->leftJoin('c.sentTimestamp', 'sent')
            ->addSelect('sent')
            ->leftJoin('c.receivedTimestamp', 'received')
            ->addSelect('received')
            ->orderBy('user.lastName')
            ->addOrderBy('user.firstName')
            ->getQuery()
            ->getResult();
    }


    public function getEveryoneInSitting(Sitting $sitting): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.sitting = :sitting')
            ->setParameter('sitting', $sitting)
            ->join('c.user', 'user')
            ->addSelect('user')
            ->join('user.role', 'role')
            ->addSelect('role')
            ->leftJoin('c.sentTimestamp', 'sent')
            ->addSelect('sent')
            ->leftJoin('c.receivedTimestamp', 'received')
            ->addSelect('received')
            ->orderBy('role.prettyName', 'ASC')
            ->addOrderBy('user.lastName', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function deleteInvitationsBySitting(Sitting $sitting): int
    {
        return $this->createQueryBuilder('c')
            ->delete()
            ->andWhere('c.sitting = :sitting')
            ->setParameter('sitting', $sitting)
            ->andWhere('c.category = :category')
            ->setParameter('category', Convocation::CATEGORY_INVITATION)
            ->getQuery()
            ->execute();
    }

    /**
     * @param array<string> $toExcludeIds
     */
    public function findAvailableMandatorsInSitting(?Sitting $sitting, User $user, array $toExcludeIds): QueryBuilder
    {
        if (in_array($user->getId(), $toExcludeIds)) {
            return $this->createQueryBuilder('c')
                ->where('0 = 1');
        }

        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.sitting = :sitting')
            ->setParameter('sitting', $sitting)
            ->innerJoin(User::class, 'u', Join::WITH, 'c.user = u')
            ->andWhere('c.category = :category')
            ->setParameter('category', Convocation::CATEGORY_CONVOCATION)
            ->andWhere('u.id != :user')
            ->setParameter('user', $user)
            ->andWhere('(c.attendance = :present) OR (c.attendance = :remote) OR (c.attendance = :undefined) OR (c.attendance IS NULL)')
            ->setParameter('present', Convocation::PRESENT)
            ->setParameter('remote', Convocation::REMOTE)
            ->setParameter('undefined', Convocation::UNDEFINED);

        if (!empty($toExcludeIds)) {
            $qb->andWhere('u.id NOT IN (:toExclude)')
                ->setParameter('toExclude', $toExcludeIds);
        }
        $qb->orderBy('u.lastName');

        return $qb;
    }

    public function findConvocationByUserAndSitting($user, $sitting): ?Convocation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->andWhere('c.sitting = :sitting')
            ->setParameter('sitting', $sitting)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
