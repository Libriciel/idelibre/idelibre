<?php

namespace App\Repository;

use App\Entity\Party;
use App\Entity\Structure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Party|null find($id, $lockMode = null, $lockVersion = null)
 * @method Party|null findOneBy(array $criteria, array $orderBy = null)
 * @method Party[]    findAll()
 * @method Party[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Party::class);
    }

    public function findByStructure(Structure $structure): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.structure =:structure')
            ->setParameter('structure', $structure)
            ->orderBy('p.name', 'ASC');
    }
}
