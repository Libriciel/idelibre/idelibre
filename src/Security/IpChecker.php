<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\IpUtils;

class IpChecker
{
    public function isIpAllowed(string $clientIp, string $ipRange): bool
    {
        return IpUtils::checkIp($clientIp, $ipRange);
    }
}
