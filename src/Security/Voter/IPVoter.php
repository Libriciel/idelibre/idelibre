<?php

namespace App\Security\Voter;

use App\Security\IpChecker;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class IPVoter extends Voter
{
    public function __construct(private readonly RequestStack $requestStack, private readonly IpChecker $ipChecker, private ParameterBagInterface $bag)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $attribute === 'ACCESS_PROFILER';
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $request = $this->requestStack->getCurrentRequest();

        // Check if the IP is allowed using IpChecker
        return $this->ipChecker->isIpAllowed($request->getClientIp(), $this->bag->get('allowed_profiler_ip'));
    }
}
