<?php

namespace App\Service\Convocation;

use App\Entity\Convocation;
use App\Entity\Sitting;
use App\Repository\ConvocationRepository;
use App\Repository\UserRepository;
use App\Service\Email\EmailNotSendException;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;

class ConvocationAttendanceManager
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly ConvocationRepository $convocationRepository,
        private readonly ConvocationManager $convocationManager,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @throws ConnectionException
     * @throws EmailNotSendException
     */
    public function updateAttendanceForDeputies(array $convocationAttendances, Sitting $sitting): void
    {
        foreach ($convocationAttendances as $convocationAttendance) {
            if ($convocationAttendance->getAttendance() === Convocation::ABSENT_SEND_DEPUTY) {
                $convocation = $this->convocationRepository->findOneBy(["id" => $convocationAttendance->getConvocationId()]);
                $deputy = $this->userRepository->findOneby(["id" => $convocationAttendance->getDeputyId()]);
                $deputyConvocation = $this->convocationRepository->findConvocationByUserAndSitting($deputy, $sitting);

                if (!$deputyConvocation) {
                    $this->convocationManager->createAndSendConvocation($deputy, $convocation);
                }

                if ($deputyConvocation and $deputyConvocation->getAttendance() === Convocation::ABSENT) {
                    $deputyConvocation->setAttendance(Convocation::PRESENT);
                    $this->entityManager->persist($deputyConvocation);
                    $this->convocationManager->sendConvocation($deputyConvocation);
                }
            }
        }

        $this->entityManager->flush();
    }

    public function resetDeputiesAttendanceToAbsent(convocation $convocation): void
    {
        $convocation->setAttendance(Convocation::ABSENT);
        $this->entityManager->persist($convocation);
        $this->entityManager->flush();
    }
}
