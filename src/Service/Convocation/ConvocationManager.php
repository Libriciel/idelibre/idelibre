<?php

namespace App\Service\Convocation;

use App\Entity\Convocation;
use App\Entity\Enum\RoleName;
use App\Entity\File;
use App\Entity\Sitting;
use App\Entity\User;
use App\Message\ConvocationSent;
use App\Repository\ConvocationRepository;
use App\Repository\UserRepository;
use App\Service\ClientNotifier\ClientNotifierInterface;
use App\Service\Email\Attachment;
use App\Service\Email\CalGenerator;
use App\Service\Email\EmailNotSendException;
use App\Service\Email\EmailServiceInterface;
use App\Service\EmailTemplate\EmailGenerator;
use App\Service\Timestamp\TimestampConvocation;
use App\Service\Timestamp\TimestampManager;
use App\Util\AttendanceTokenUtil;
use Doctrine\Common\Proxy\Proxy;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

class ConvocationManager
{
    public function __construct(
        private readonly EntityManagerInterface  $em,
        private readonly ConvocationRepository   $convocationRepository,
        private readonly TimestampManager        $timestampManager,
        private readonly LoggerInterface         $logger,
        private readonly ParameterBagInterface   $bag,
        private readonly EmailServiceInterface   $emailService,
        private readonly EmailGenerator          $emailGenerator,
        private readonly UserRepository          $userRepository,
        private readonly ClientNotifierInterface $clientNotifier,
        private readonly MessageBusInterface     $messageBus,
        private readonly CalGenerator            $icalGenerator,
        private readonly AttendanceTokenUtil     $attendanceTokenUtil,
        private readonly TimestampConvocation    $timestampConvocation,
    ) {
    }

    public function createConvocationsActors(Sitting $sitting): void
    {
        $associatedActors = $this->userRepository->getAssociatedActorsWithType($sitting->getType());
        $this->createConvocations($sitting, $associatedActors);
    }

    public function createConvocationsInvitableEmployees(Sitting $sitting): void
    {
        $associatedInvitableEmployees = $this->userRepository->getAssociatedInvitableEmployeesWithType($sitting->getType());
        $this->createConvocations($sitting, $associatedInvitableEmployees);
    }

    public function createConvocationsGuests(Sitting $sitting): void
    {
        $associatedGuests = $this->userRepository->getAssociatedGuestWithType($sitting->getType());
        $this->createConvocations($sitting, $associatedGuests);
    }

    public function createConvocationsDeputies(Sitting $sitting, array $deputies): void
    {
        $this->createConvocations($sitting, $deputies);
        $this->em->flush();
    }

    public function createConvocationForNewlyAssignedDeputies(Sitting $sitting): void
    {
        $newlyAssignedDeputies = $this->newlyAssignedDeputies($sitting);
        $this->createConvocations($sitting, $newlyAssignedDeputies);
        $this->em->flush();
    }


    /**
     * @param User[] $associatedUsers
     * @throws Exception
     */
    private function createConvocations(Sitting $sitting, array $associatedUsers): void
    {
        foreach ($associatedUsers as $user) {
            if (!$this->alreadyHasConvocation($user, $sitting)) {
                $convocation = (new Convocation())
                    ->setSitting($sitting)
                    ->setUser($user)
                    ->setCategory($this->getConvocationCategory($user))
                    ->setAttendanceToken($this->attendanceTokenUtil->prepareToken($sitting->getDate()))
                    ->setDeputy(null)
                    ->setAttendance($user->getRole()->getName() === RoleName::NAME_ROLE_DEPUTY ? Convocation::ABSENT : Convocation::UNDEFINED);
                ;

                $this->em->persist($convocation);
            }
        }
    }

    private function getConvocationCategory(User $user): string
    {
        if (RoleName::NAME_ROLE_ACTOR === $user->getRole()->getName()) {
            return Convocation::CATEGORY_CONVOCATION;
        }

        if (RoleName::NAME_ROLE_DEPUTY === $user->getRole()->getName()) {
            return Convocation::CATEGORY_DELEGATION;
        }

        return Convocation::CATEGORY_INVITATION;
    }

    /**
     * @param User[] $users
     */
    public function addConvocations(iterable $users, Sitting $sitting): void
    {
        foreach ($users as $user) {
            if ($this->alreadyHasConvocation($user, $sitting)) {
                continue;
            }
            $convocation = new Convocation();
            $convocation->setSitting($sitting)
                ->setUser($user)
                ->setAttendanceToken($this->attendanceTokenUtil->prepareToken($sitting->getDate()))
                ->setCategory($this->getConvocationCategory($user))
                ->setDeputy(null);
            $this->em->persist($convocation);

            if ($user->getDeputy()) {
                $convocationDeputy = new Convocation();
                $convocationDeputy->setSitting($sitting)
                    ->setUser($user->getDeputy())
                    ->setAttendanceToken($this->attendanceTokenUtil->prepareToken($sitting->getDate()))
                    ->setCategory($this->getConvocationCategory($user->getDeputy()))
                    ->setDeputy(null);
                $this->em->persist($convocationDeputy);
            }
        }
        $this->em->flush();
    }

    /**
     * @param Convocation[] $convocations
     */
    public function deleteConvocationsNotSent(iterable $convocations): void
    {
        foreach ($convocations as $convocation) {
            if ($this->isAlreadySent($convocation)) {
                continue;
            }
            $user = $convocation->getUser();
            if ($user->getDeputy()) {
                $convocationDeputy = $this->convocationRepository->findOneBy(['user' => $user->getDeputy(), 'sitting' => $convocation->getSitting()]);
                if ($convocationDeputy) {
                    $this->em->remove($convocationDeputy);
                }
            }

            $this->em->remove($convocation);
        }
        $this->em->flush();
    }

    private function alreadyHasConvocation(User $user, Sitting $sitting): bool
    {
        $convocation = $this->convocationRepository->findOneBy(['user' => $user, 'sitting' => $sitting]);

        return !empty($convocation);
    }

    /**
     * NB le processe d'envoi et d'hrodatage pourrait (devrait) ce faire en async.
     *
     * @throws ConnectionException
     * @throws EmailNotSendException
     */
    public function sendAllConvocations(Sitting $sitting, ?string $userProfile): void
    {
        $convocations = $this->getConvocationNotSentByUserProfile($sitting, $userProfile);
        $isActiveUserConvocations = [];

        foreach ($convocations as $convocation) {
            if ($convocation->getUser()->getIsActive()) {
                $isActiveUserConvocations[] = $convocation;
            }
        }

        while (count($isActiveUserConvocations)) {
            $convocationBatch = array_splice($isActiveUserConvocations, 0, $this->bag->get('max_batch_email'));
            $this->timestampAndActiveConvocations($sitting, $convocationBatch);
            $this->clientNotifier->newSittingNotification($convocationBatch);
            $emails = $this->generateEmailsData($sitting, $convocationBatch);
            $this->emailService->sendBatch($emails);
            $this->messageBus->dispatch(
                new ConvocationSent(
                    array_map(fn (Convocation $c) => $c->getId(), $convocationBatch),
                    $sitting->getId()
                )
            );
        }
    }

    /**
     * @throws ConnectionException
     * @throws EmailNotSendException
     */
    public function sendConvocation(Convocation $convocation)
    {
        $hasTimestamps = $convocation->getSentTimestamp() !== null;

        if ($hasTimestamps) {
            $this->timestampConvocation->createResendTimestamp($convocation->getSitting(), $convocation);
        }
        if (!$hasTimestamps) {
            $this->timestampAndActiveConvocations($convocation->getSitting(), [$convocation]);
        }

        $emails = $this->generateEmailsData($convocation->getSitting(), [$convocation]);

        $this->clientNotifier->newSittingNotification([$convocation]);
        $this->emailService->sendBatch($emails);
        $this->messageBus->dispatch(new ConvocationSent([$convocation->getId()], $convocation->getSitting()->getId()));
    }


    /**
     * @throws ConnectionException
     */
    private function timestampAndActiveConvocations(Sitting $sitting, iterable $convocations): bool
    {
        $this->em->getConnection()->beginTransaction();
        try {
            $notSentConvocations = $this->filterNotSentConvocations($convocations);
            $timeStamp = $this->timestampConvocation->createConvocationTimestamp($sitting, $notSentConvocations);

            foreach ($notSentConvocations as $convocation) {
                $convocation->setIsActive(true)
                    ->setSentTimestamp($timeStamp);

                $this->em->persist($convocation);
            }
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->error($e->getMessage());

            return false;
        }

        return true;
    }

    private function filterNotSentConvocations(iterable $convocations): array
    {
        $notSent = [];
        foreach ($convocations as $convocation) {
            if (!$this->isAlreadySent($convocation)) {
                $notSent[] = $convocation;
            }
        }

        return $notSent;
    }

    private function isAlreadySent(Convocation $convocation): bool
    {
        return (bool)$convocation->getSentTimestamp();
    }

    /**
     * @param iterable<Convocation> $convocations
     */
    public function deleteConvocations(iterable $convocations): void
    {
        foreach ($convocations as $convocation) {
            $this->em->remove($convocation);
            $this->deleteAssociatedTimestamp($convocation);
        }

        $this->clientNotifier->removedSittingNotification($convocations->toArray());
    }

    private function deleteAssociatedTimestamp(Convocation $convocation): void
    {
        if ($convocation->getSentTimestamp()) {
            $this->timestampManager->delete($convocation->getSentTimestamp());
        }
        if ($convocation->getReceivedTimestamp()) {
            $this->timestampManager->delete($convocation->getReceivedTimestamp());
        }
    }

    /**
     * @param Convocation[] $convocations
     */
    public function deactivate(iterable $convocations): void
    {
        foreach ($convocations as $convocation) {
            $convocation->setIsActive(false);
            $this->em->persist($convocation);
        }
        $this->clientNotifier->removedSittingNotification($convocations->toArray());
    }

    /**
     * @param iterable<Convocation> $convocations
     */
    public function reactivate(iterable $convocations): void
    {
        foreach ($convocations as $convocation) {
            $convocation->setIsActive(true);
            $this->em->persist($convocation);
        }
    }

    /**
     * @param Convocation[] $convocations
     */
    private function generateEmailsData(Sitting $sitting, array $convocations): array
    {
        $emails = [];
        foreach ($convocations as $convocation) {
            if ($convocation->getUser()->getIsActive()) {
                $email = $this->emailGenerator->generateFromTemplateAndConvocation($sitting->getType()->getEmailTemplate(), $convocation);
                $email->setTo($convocation->getUser()->getEmail());
                $email->setReplyTo($sitting->getStructure()->getReplyTo());
                $email->setAttachment($this->getConvocationAttachment($convocation, $sitting));

                $email->setCalPath($this->icalGenerator->generate($sitting));

                $emails[] = $email;
            }
        }

        return $emails;
    }

    private function getConvocationNotSentByUserProfile(Sitting $sitting, ?string $userProfile): array
    {
        switch ($userProfile) {
            case RoleName::NAME_ROLE_ACTOR:
                $convocations = $this->convocationRepository->getActorConvocationsBySitting($sitting);
                break;
            case RoleName::NAME_ROLE_GUEST:
                $convocations = $this->convocationRepository->getGuestConvocationsBySitting($sitting);
                break;
            case RoleName::NAME_ROLE_EMPLOYEE:
                $convocations = $this->convocationRepository->getInvitableEmployeeConvocationsBySitting($sitting);
                break;
            default:
                $convocations = $this->convocationRepository->getEveryOneButDeputyConvocationsBySitting($sitting);
        }

        return $this->keepOnlyNotSent($convocations);
    }

    /**
     * @param array<Convocation> $convocations
     */
    public function keepOnlyNotSent(array $convocations): array
    {
        $notSentConvocations = [];
        foreach ($convocations as $convocation) {
            if (!$convocation->getSentTimestamp()) {
                $notSentConvocations[] = $convocation;
            }
        }

        return $notSentConvocations;
    }

    /**
     * @param array<ConvocationAttendance> $convocationAttendances
     */
    public function updateConvocationAttendances(array $convocationAttendances): void
    {
        foreach ($convocationAttendances as $convocationAttendance) {
            $convocation = $this->convocationRepository->find($convocationAttendance->getConvocationId());
            if (!$convocation) {
                throw new NotFoundHttpException("Convocation with id ${convocationAttendance['convocationId']} does not exists");
            }
            // TODO check si le remote est autorisé
            $convocation->setAttendance($convocationAttendance->getAttendance());
            $convocation->setDeputy($convocationAttendance->getDeputyId() ? $this->userRepository->find($convocationAttendance->getDeputyId()) : null);
            $convocation->setMandator($convocationAttendance->getMandataireId() ? $this->userRepository->find($convocationAttendance->getMandataireId()) : null);

            $this->em->persist($convocation);
        }
        $this->em->flush();
    }

    private function getConvocationAttachment(Convocation $convocation, Sitting $sitting): Attachment
    {
        $file = $this->getConvocationFile($convocation, $sitting);

        return new Attachment($file->getName(), $file->getPath());
    }

    private function getConvocationFile(Convocation $convocation, Sitting $sitting): ?File
    {
        if (Convocation::CATEGORY_CONVOCATION === $convocation->getCategory() || Convocation::CATEGORY_DELEGATION === $convocation->getCategory()) {
            return $sitting->getConvocationFile();
        }

        return $sitting->getInvitationFile();
    }

    public function countConvocationNotanswered(iterable $convocations): int
    {
        $count = 0;
        foreach ($convocations as $convocation) {
            if ($this->isActorConvocation($convocation)) {
                if ($convocation->getAttendance() === "" || $convocation->getAttendance() === null) {
                    $count++;
                }
            }
        }
        return $count;
    }

    private function isActorConvocation(Convocation $convocation)
    {
        return $convocation->getUser()->getRole()->getName() === RoleName::NAME_ROLE_ACTOR;
    }

    public function markAsRead(Convocation $convocation): void
    {
        $timeStamp = $this->timestampConvocation->createConvocationReceivedTimestamp($convocation);
        $convocation->setIsRead(true);
        $convocation->setReceivedTimestamp($timeStamp);
        $this->em->persist($convocation);
        $this->em->flush();
    }

    public function getAlreadyMandatorBySitting(array $convocations): array
    {
        $alreadyMandator = [];
        foreach ($convocations as $convocation) {
            if ($convocation->getAttendance() === Convocation::ABSENT_GIVE_POA && $convocation->getMandator() !== null) {
                $alreadyMandator[] = $convocation->getMandator()->getId();
            }
        }
        return $alreadyMandator;
    }

    /**
     * @return array<User>
     */
    public function getAvailableMandatorConvocationsBySitting(Sitting $sitting): array
    {
        $convocations = $this->convocationRepository->getActorConvocationsBySitting($sitting);
        $alreadyMandator = $this->getAlreadyMandatorBySitting($convocations);
        $availableMandatorConvocations = [];
        foreach ($convocations as $convocation) {
            if (!in_array($convocation->getUser()->getId(), $alreadyMandator)) {
                $availableMandatorConvocations[] = $convocation;
            }
        }
        return $availableMandatorConvocations;
    }


    /**
     * @param array<ConvocationAttendance> $convocationAttendances
     */
    public function validateConvocationAttendancesMandator(array $convocationAttendances): void
    {
        if (empty($convocationAttendances)) {
            return;
        }

        $convocation = $this->convocationRepository->find($convocationAttendances[0]->getConvocationId());
        $currentConvocations = $this->convocationRepository->findBy(['sitting' => $convocation->getSitting()]);

        $currentConvocationsWithMandatorIds = array_values(array_filter($currentConvocations, fn ($convocation) => $convocation->getMandator() !== null));
        $currentConvocationsWithMandatorIds = array_map(fn ($convocation) => [$convocation->getId() => $convocation->getMandator()->getId()], $currentConvocationsWithMandatorIds);

        $addedMandataireIds = array_map(fn ($convoc) => [$convoc->getConvocationId() => $convoc->getMandataireId()], $convocationAttendances);

        $actualMandators = array_merge(...$currentConvocationsWithMandatorIds, ...$addedMandataireIds);
        $actualMandatorsValues = array_filter(array_values($actualMandators));


        if (count($actualMandatorsValues) !== count(array_unique($actualMandatorsValues))) {
            throw new BadRequestHttpException('MandataireId must be unique');
        }
    }


    public function getDeputiestoExclude(Convocation $convocation): array
    {
        $actorsInStructure = $this->userRepository->findActorsInStructure($convocation->getSitting()->getStructure())->getQuery()->getResult();
        $currentDeputy = $convocation->getUser()->getDeputy();
        $deputyAssignedForSitting = $convocation->getDeputy();

        $assignedDeputies = [];
        foreach ($actorsInStructure as $actor) {
            if ($actor->getDeputy() and $actor->getDeputy() !== $currentDeputy) {
                $assignedDeputies[] = $actor->getDeputy();
            }
        }

        foreach ($convocation->getSitting()->getConvocations() as $convocation) {
            if ($convocation->getDeputy() and !in_array($convocation->getDeputy(), $assignedDeputies) and $convocation->getDeputy() !== $deputyAssignedForSitting) {
                $assignedDeputies = [...$assignedDeputies, $convocation->getDeputy()];
            }
        }

        return $assignedDeputies;
    }

    public function newlyAssignedDeputies(Sitting $sitting): array
    {
        $convocationsBySitting = $this->convocationRepository->getActorConvocationsBySitting($sitting);
        $newlyAssignedDeputies = [];
        foreach ($convocationsBySitting as $convocation) {
            if ($convocation->getDeputy()) {
                $newlyAssignedDeputies[] = $convocation->getDeputy();
            }
        }
        return $newlyAssignedDeputies;
    }

    public function removeDeputyConvocation(?Convocation $convocation): void
    {
        if (!$convocation) {
            return;
        }

        $this->em->remove($convocation);
        $this->em->flush();
    }


    /**
     * @param Convocation $convocation
     * @return User|null
     */
    public function findPreviousDeputyIfAny(Convocation $convocation): ?User
    {
        $deputyFromConvocation = $convocation->getDeputy() ? $this->userRepository->findOneBy(["id" => $convocation->getDeputy()]) : null;
        $deputyFromUser = $convocation->getUser()->getDeputy() ? $this->userRepository->findOneBy(["id" => $convocation->getUser()->getDeputy()]) : null;

        match (true) {
            $deputyFromConvocation === null && $deputyFromUser === null => $previousDeputy = null,
            $deputyFromConvocation === null && $deputyFromUser !== null => $previousDeputy = $deputyFromUser,
            $deputyFromConvocation !== null && $deputyFromUser === null => $previousDeputy = $deputyFromConvocation,
            $deputyFromConvocation !== null && $deputyFromUser !== null => $previousDeputy = $deputyFromConvocation,
            default => $previousDeputy = null,
        };
        return $previousDeputy;
    }


    public function updateDeputyConvocation(?User $previousDeputy, ?User $newDeputy, Convocation $convocation): void
    {
        switch (true) {
            case $previousDeputy === null && $newDeputy === null:
                break;
            case $previousDeputy === null && $newDeputy !== null:
                $this->createAndSendConvocation($newDeputy, $convocation);
                $this->updatePresence($newDeputy, Convocation::PRESENT, $convocation->getSitting());
                break;
            case $previousDeputy !== null && $newDeputy === null:

                $oldConvocation = $this->convocationRepository->findConvocationByUserAndSitting($previousDeputy, $convocation->getSitting());
                if ($oldConvocation) {
                    $oldConvocation->setAttendance(Convocation::ABSENT);
                    break;
                }
                $this->createAndSendConvocation($previousDeputy, $convocation);
                $this->updatePresence($previousDeputy, Convocation::PRESENT, $convocation->getSitting());
                break;
            case $previousDeputy === $convocation->getUser()->getDeputy() && $newDeputy !== null:
                $this->createAndSendConvocation($newDeputy, $convocation);
                $this->updatePresence($previousDeputy, Convocation::ABSENT, $convocation->getSitting());
                $this->updatePresence($newDeputy, Convocation::PRESENT, $convocation->getSitting());
                break;
            case $previousDeputy !== $convocation->getUser()->getDeputy() && ($previousDeputy !== null && $newDeputy !== null):
                $this->createAndSendConvocation($newDeputy, $convocation);
                $this->updatePresence($previousDeputy, Convocation::ABSENT, $convocation->getSitting());
                $this->updatePresence($newDeputy, Convocation::PRESENT, $convocation->getSitting());
                break;
        }
        //        dd('end');
    }


    public function getQuorum(Sitting $sitting): string
    {
        $convocations = $this->convocationRepository->getActorConvocationsBySitting($sitting);
        $quorum = 0;
        foreach ($convocations as $convocation) {
            $this->isQuorum($convocation) ? $quorum++ : 0;
        }
        return $quorum . '/' . count($convocations);
    }

    private function isQuorum(Convocation $convocation): string
    {
        return
            $convocation->getAttendance() === Convocation::PRESENT
            || $convocation->getAttendance() === Convocation::ABSENT_GIVE_POA
            || $convocation->getAttendance() === Convocation::ABSENT_SEND_DEPUTY
            || $convocation->getAttendance() === Convocation::REMOTE
        ;
    }

    public function getInTheRoom(Sitting $sitting): string
    {
        $convocations = $this->convocationRepository->getActorConvocationsBySitting($sitting);
        $inTheRoom = 0;
        foreach ($convocations as $convocation) {
            $convocation->getAttendance() === Convocation::PRESENT ? $inTheRoom++ : 0;
        }

        return $inTheRoom . '/' . count($convocations);
    }

    public function getAssignedandPresentDeputy(Sitting $sitting): string
    {
        $deputies = $this->convocationRepository->getDeputyConvocationsBySitting($sitting);
        $presentDeputies = 0;
        foreach ($sitting->getConvocations() as $convocation) {
            $convocation->getAttendance() === Convocation::ABSENT_SEND_DEPUTY ? $presentDeputies++ : 0;
        }

        return $presentDeputies . '/' . count($deputies);
    }

    public function getPoa(Sitting $sitting): string // poa = power of attorney
    {
        $convocations = $this->convocationRepository->getActorConvocationsBySitting($sitting);
        $poa = 0;
        foreach ($convocations as $convocation) {
            $convocation->getAttendance() === Convocation::ABSENT_GIVE_POA ? $poa++ : 0;
        }

        return $poa . '/' . count($convocations);
    }
    private function createAndSendConvocation(User $deputy, Convocation $convocation): void
    {
        $this->createConvocationsDeputies($convocation->getSitting(), [$deputy]);

        $deputyConvocation = $this->convocationRepository->findConvocationByUserAndSitting($deputy, $convocation->getSitting());

        $deputyConvocation->setSentTimestamp(null); // getSentTimestamp can't be access before initialization
        $deputyConvocation->setTitular($convocation->getUser());
        $deputyConvocation->setAttendance(Convocation::PRESENT);

        $this->em->persist($deputyConvocation);
        $this->em->flush();

        $this->sendConvocation($deputyConvocation);
    }

    public function updatePresence(User $deputy, string $attendance, Sitting $sitting): void
    {
        $convocation = $this->convocationRepository->findConvocationByUserAndSitting($deputy, $sitting);
        $convocation->setAttendance($attendance);
        $this->em->persist($convocation);
        $this->em->flush();
    }
}
