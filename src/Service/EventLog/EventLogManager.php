<?php

namespace App\Service\EventLog;

use App\Entity\EventLog\Action;
use App\Entity\EventLog\EventLog;
use App\Repository\StructureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;

class EventLogManager
{
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
        private readonly StructureRepository $structureRepository
    ) {
    }

    public function createLog(Action $action, string $targetId, string $targetName, string $structureId = null, bool $isFlush = true): EventLog
    {
        $eventLog = (new EventLog())
            ->setAction($action)
            ->setTargetId($targetId)
            ->setTargetName($targetName)
            ->setStructureId($structureId);

        $this->entityManager->persist($eventLog);

        $eventLog
            ->setAuthorId($this->getAuthorIdCase($eventLog))
            ->setAuthorName($this->getAuthorNameCase($eventLog));

        if ($isFlush) {
            $this->saveLog($eventLog);
        }

        return $eventLog;
    }

    public function saveLog(EventLog $eventLog): void
    {
        $this->entityManager->persist($eventLog);
        $this->entityManager->flush();
    }

    private function isModifiedPasswordAction(EventLog $eventLog): bool
    {
        $structure = $this->structureRepository->findOneBy(["id" => $eventLog->getStructureId()]);


        return $eventLog->getAction() === Action::USER_PASSWORD_UPDATED && str_contains($eventLog->getTargetName(), '@' . strtolower($structure?->getName() ?? ""));
    }

    private function getAuthorIdCase(EventLog $eventLog): string
    {
        if (!$this->isModifiedPasswordAction($eventLog)) {
            return $this->security->getUser() !== null ? $this->security->getUser()->getId() : 'SCRIPT';
        }
        return match ($this->isModifiedPasswordAction($eventLog)) {
            $this->security->getUser() !== null => $this->security->getUser()->getId(),
            $eventLog->getAuthorId() === null && $eventLog->getTargetId() !== null => $eventLog->getTargetId(),
            default => 'SCRIPT',
        };
    }


    private function getAuthorNameCase(EventLog $eventLog): string
    {
        if (!$this->isModifiedPasswordAction($eventLog)) {
            return $this->security->getUser() !== null ? $this->security->getUser()->getUsername() : 'SCRIPT';
        }

        return match ($this->isModifiedPasswordAction($eventLog)) {
            $this->security->getUser() !== null => $this->security->getUser()->getUsername(),
            $eventLog->getAuthorName() === null && $eventLog->getTargetName() !== null => $eventLog->getTargetName(),
            default => 'SCRIPT',
        };
    }
}
