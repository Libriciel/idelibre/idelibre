<?php

namespace App\Service\File\Generator;

use Exception;
use Throwable;

class FileCheckerException extends Exception
{
    public string $pdfName;

    public function __construct(string $pdfName, string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->pdfName = $pdfName;
    }
}
