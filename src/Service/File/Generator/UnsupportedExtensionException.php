<?php

namespace App\Service\File\Generator;

use Exception;

class UnsupportedExtensionException extends Exception
{
}
