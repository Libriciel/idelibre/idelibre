<?php

namespace App\Service\Otherdoc;

use App\Entity\Otherdoc;
use App\Entity\Role;
use App\Repository\OtherdocRepository;
use App\Service\role\RoleManager;

class OtherdocProvider
{
    public function __construct(private readonly OtherdocRepository $otherdocRepository, private readonly RoleManager $roleManager)
    {
    }


    /**
     * @return array<Otherdoc>
     */
    public function getOtherdocsBySitting($sitting, Role $role): array
    {
        if ($this->roleManager->getGuestRole()->getId() == $role->getId()) {
            return [];
        }

        return $this->otherdocRepository->getOtherdocsBySitting($sitting);
    }
}
