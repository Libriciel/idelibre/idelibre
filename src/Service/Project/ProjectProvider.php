<?php

namespace App\Service\Project;

use App\Entity\Project;
use App\Entity\Role;
use App\Repository\ProjectRepository;
use App\Service\role\RoleManager;

class ProjectProvider
{
    public function __construct(private readonly ProjectRepository $projectRepository, private readonly RoleManager $roleManager)
    {
    }


    /**
     * @return array<Project>
     */
    public function getProjectsBySitting($sitting, Role $role): array
    {
        if ($this->roleManager->getGuestRole()->getId() == $role->getId()) {
            return [];
        }

        return $this->projectRepository->getProjectsBySitting($sitting);
    }
}
