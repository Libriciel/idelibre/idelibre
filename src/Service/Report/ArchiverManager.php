<?php

namespace App\Service\Report;

use App\Entity\Sitting;
use App\Service\File\Generator\FileCheckerException;
use App\Service\File\Generator\FileGenerator;
use App\Service\File\Generator\SittingZipGenerator;
use App\Service\File\Generator\UnsupportedExtensionException;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ArchiverManager
{
    public function __construct(
        private readonly PdfSittingReport $pdfSittingReport,
        private readonly CsvSittingReport $csvSittingReport,
        private readonly FileGenerator    $fileGenerator,
        private readonly SittingZipGenerator $sittingZipGenerator
    ) {
    }

    /**
     * @throws UnsupportedExtensionException
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     * @throws ArchiverException
     * @throws FileCheckerException
     */
    public function generateFinalAndCopy(Sitting $sitting): void
    {
        $generatedPdfPath = $this->pdfSittingReport->generate($sitting);
        $this->copyGeneratedFile($generatedPdfPath, $sitting, 'pdf');
        $generatedCsvPath = $this->csvSittingReport->generate($sitting);
        $this->copyGeneratedFile($generatedCsvPath, $sitting, 'csv');
    }


    /**
     * @throws UnsupportedExtensionException
     * @throws ArchiverException
     */
    private function copyGeneratedFile(string $generatedPdfPath, Sitting $sitting, string $extension): void
    {
        $directoryPath = $this->fileGenerator->getDirectoryPathByExtension($extension) . $sitting->getStructure()->getId();
        $finalPath = $directoryPath . '/' . $sitting->getId() . '_report_' . $extension . '_sitting.' . $extension;

        $fileSystem = new Filesystem();

        if ($extension === 'csv') {
            if (!file_exists($directoryPath)) {
                $fileSystem->mkdir($directoryPath);
            }
        }

        if ($fileSystem->exists($finalPath)) {
            throw new ArchiverException('La séance ayant déja été archivée, les rapports pdf et csv de la séance existent déjà dans leur état final. Ils n\'ont pas été regénéré.');
        }

        $fileSystem->copy($generatedPdfPath, $finalPath);
    }
}
