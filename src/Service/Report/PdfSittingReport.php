<?php

namespace App\Service\Report;

use App\Entity\Convocation;
use App\Entity\Sitting;
use App\Repository\ConvocationRepository;
use App\Service\Convocation\ConvocationManager;
use App\Service\File\Generator\FileGenerator;
use App\Service\File\Generator\UnsupportedExtensionException;
use Knp\Snappy\Pdf;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class PdfSittingReport
{
    public function __construct(

        private readonly FileGenerator $fileGenerator,
        private readonly Pdf                   $pdf,
        private readonly Environment           $twig,
        private readonly ConvocationRepository $convocationRepository,
        private readonly ConvocationManager $convocationManager
    ) {
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function generate(Sitting $sitting): string
    {
        $html = $this->twig->render('generate/sitting_report_pdf.html.twig', [
            'convocations' => $this->convocationRepository->getEveryoneInSitting($sitting),
            'sitting' => $sitting,
            'timezone' => $sitting->getStructure()->getTimezone()->getName(),
            'attendance' => [
                Convocation::PRESENT => 'Présent',
                Convocation::ABSENT => 'Absent',
                Convocation::REMOTE => 'Distanciel',
                Convocation::ABSENT_GIVE_POA => 'Donne pouvoir',
                Convocation::ABSENT_SEND_DEPUTY => 'Envoie son suppléant',
            ],
            'inTheRoom' => $this->convocationManager->getInTheRoom($sitting),
            'quorum' => $this->convocationManager->getQuorum($sitting),
            'poa' => $this->convocationManager->getPoa($sitting),
            'deputiesPresent' => $this->convocationManager->getAssignedandPresentDeputy($sitting),
        ]);

        $generatedPdfPath = '/tmp/' . $sitting->getId() . '_report_pdf_sitting.pdf';

        if (file_exists($generatedPdfPath)) {
            $this->deleteGeneratedFile($generatedPdfPath);
        }

        $this->pdf->generateFromHtml($html, $generatedPdfPath, [
            'orientation' => 'landscape',
            'footer-right' => '[page] / [toPage]',
        ]);

        return $generatedPdfPath;
    }


    /**
     * @throws UnsupportedExtensionException
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     * @throws PdfReportException
     */
    public function generateFinalAndCopy(Sitting $sitting): void
    {
        $generatedPdfPath = $this->generate($sitting);
        $this->copyGeneratedFile($generatedPdfPath, $sitting);
    }


    /**
     * @throws UnsupportedExtensionException
     * @throws PdfReportException
     */
    private function copyGeneratedFile(string $generatedPdfPath, Sitting $sitting): void
    {
        $directoryPath = $this->fileGenerator->getDirectoryPathByExtension('pdf') . $sitting->getStructure()->getId();
        $finalPath = $directoryPath . '/' . $sitting->getId() . '_report_pdf_sitting.pdf';

        $fileSystem = new Filesystem();

        if ($fileSystem->exists($finalPath)) {
            throw new PdfReportException('Le rapport pdf et csv de la séance existent déjà, ils ne seront pas regénérés.');
        }

        $fileSystem->copy($generatedPdfPath, $finalPath);
    }

    public function deleteGeneratedFile(string $generatedPdfPath): void
    {
        $fileSystem = new Filesystem();
        $fileSystem->remove($generatedPdfPath);
    }
}
