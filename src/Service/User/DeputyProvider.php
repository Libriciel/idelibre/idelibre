<?php

namespace App\Service\User;

use App\Entity\Convocation;
use App\Entity\Sitting;
use App\Entity\User;
use App\Repository\ConvocationRepository;
use App\Repository\UserRepository;

class DeputyProvider
{
    public function __construct(
        private readonly ConvocationRepository $convocationRepository,
        private readonly UserRepository        $userRepository
    ) {
    }


    /**
     * @param Sitting $sitting
     * @return array<User>
     */
    public function getDeputiesBySitting(Sitting $sitting): array
    {
        $convocationActors = $this->convocationRepository->getActorConvocationsBySitting($sitting);
        $actorIds = array_map(fn ($convocation) => $convocation->getUser()->getId(), $convocationActors);
        return $this->userRepository->findDeputiesByActorIds($actorIds)->getQuery()->getResult();
    }
}
