<?php

namespace App\Service\role;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RoleUpdater
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly RoleManager $roleManager)
    {
    }

    /**
     * @throws BadRequestHttpException
     */
    public function changeActorToDeputy(User $user): void
    {
        if ($user->getRole()->getId() !== $this->roleManager->getActorRole()->getId()) {
            throw new BadRequestHttpException("Original role must by actor");
        }

        if ($user->getDeputy()) {
            $user->setDeputy(null);
        }

        $user->setRole($this->roleManager->getDeputyRole());
        $this->entityManager->flush();
    }
}
