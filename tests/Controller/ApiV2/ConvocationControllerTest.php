<?php

declare(strict_types=1);

namespace App\Tests\Controller\ApiV2;

use App\Tests\LoginTrait;
use App\Tests\Story\ApiUserStory;
use App\Tests\Story\ConvocationStory;
use App\Tests\Story\EmailTemplateStory;
use App\Tests\Story\SittingStory;
use App\Tests\Story\StructureStory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ConvocationControllerTest extends WebTestCase
{
    use Factories;
    use ResetDatabase;
    use LoginTrait;

    private ?KernelBrowser $client;
    private ObjectManager $entityManager;
    private SerializerInterface $serializer;
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        self::ensureKernelShutdown();
        $this->client = static::createClient();
    }
    public function testSendConvocation()
    {
        EmailTemplateStory::load();
        $structure = StructureStory::libriciel();
        $apiUser = ApiUserStory::apiAdminLibriciel();
        SittingStory::sittingConseilLibriciel();

        $convocation = ConvocationStory::convocationActor1();

        $this->client->request(
            Request::METHOD_POST,
            '/api/v2/structures/' . $structure->getId() . '/convocations/' . $convocation->getId() . '/sendOne',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => $apiUser->getToken(),
            ],
        );

        $this->assertResponseStatusCodeSame(200);
    }
}
