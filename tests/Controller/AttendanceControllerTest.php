<?php

namespace App\Tests\Controller;

use App\Entity\Convocation;
use App\Service\Util\GenderConverter;
use App\Tests\Factory\AttendanceTokenFactory;
use App\Tests\Factory\ConvocationFactory;
use App\Tests\Factory\SittingFactory;
use App\Tests\Factory\UserFactory;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use App\Tests\Story\ConvocationStory;
use App\Tests\Story\EmailTemplateStory;
use App\Tests\Story\PartyStory;
use App\Tests\Story\RoleStory;
use App\Tests\Story\SittingStory;
use App\Tests\Story\StructureStory;
use App\Tests\Story\UserStory;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class AttendanceControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;
    use LoginTrait;
    use FindEntityTrait;

    private ?KernelBrowser $client;
    private EntityManager $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel
            ->getContainer()
            ->get('doctrine')->getManager();

        self::ensureKernelShutdown();
        $this->client = static::createClient();

        RoleStory::load();
    }


    public function testAttendanceIsPresent()
    {
        $userNoDeputy = UserFactory::createOne([
            'username' => 'actorNoDeputy@libriciel',
            'email' => 'actorNoDeputy@example.org',
            'password' => UserStory::PASSWORD,
            'firstName' => 'John',
            'lastName' => 'Doe',
            'gender' => GenderConverter::MALE,
            'title' => 'Mr le maire',
            'structure' => StructureStory::libriciel(),
            'role' => RoleStory::actor(),
            'party' => PartyStory::majorite(),
            'deputy' => null
        ]);

        $convocation = ConvocationFactory::createOne([
            'sitting' => SittingStory::sittingConseilWithTokenSent(),
            'user' => $userNoDeputy,
            'category' => Convocation::CATEGORY_CONVOCATION,
            'sentTimestamp' => null,
        ]);

        AttendanceTokenFactory::createOne([
            'token' => 'mytoken',
            'convocation' => $convocation,
        ]);
        $token = $convocation->getAttendancetoken()->getToken();

        $this->loginAsUserMontpellier();
        $crawler = $this->client->request(Request::METHOD_GET, '/attendance/confirmation/' . $token);
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('title')->text("Confirmation de présence à la séance");
        $this->assertSame("Confirmation de présence à la séance", $item);

        $form = $crawler->selectButton('Enregistrer')->form();
        $form['attendance[attendance]'] = Convocation::PRESENT;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $alert = $crawler->filter('section')->children('div.alert')->count();
        $this->assertSame(1, $alert);

        $this->assertNotEmpty($this->getOneEntityBy(Convocation::class, ['attendance' => 'present']));
    }

    public function testAttendanceIsReplacedbyMandator()
    {
        $convocationType = Convocation::CATEGORY_CONVOCATION;
        $role = RoleStory::actor();
        $sitting = SittingFactory::createOne([
            'structure' => StructureStory::libriciel(),
            'isRemoteAllowed' => false,
            'isMandatorAllowed' => true,
        ])->object();

        $convocation = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => UserFactory::createOne(["role" => $role]),
            'category' => $convocationType,
            'attendance' => Convocation::UNDEFINED,
            'sentTimestamp' => null,
            'mandator' => null,
            'deputy' => null
        ])->object();

        $convocation2 = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => UserFactory::createOne(["role" => $role]),
            'category' => $convocationType,
            'attendance' => Convocation::PRESENT,
            'sentTimestamp' => null,
            'mandator' => null,
            'deputy' => null
        ])->object();



        $tokenConvocation = AttendanceTokenFactory::createOne([
            'token' => 'mytoken',
            'convocation' => $convocation,
        ])->object();
        $tokenConvocation2 = AttendanceTokenFactory::createOne([
            "token" => "mytoken2",
            "convocation" => $convocation2
        ])->object();

        $token = $tokenConvocation->getToken();

        $this->loginAsUserMontpellier();

        $crawler = $this->client->request(
            Request::METHOD_GET,
            '/attendance/confirmation/' . $token
        );
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Merci de confirmer votre présence")');

        $this->assertCount(1, $item);

        $select = $crawler->filter('select[name="attendance[mandataire]"]')->getNode(0);
        $select->appendChild(new \DOMElement('option', 'New Option 1'))->setAttribute('value', $convocation2->getUser()->getId());
        ;

        $form = $crawler->selectButton('Enregistrer')->form();
        $form['attendance[attendance]'] = Convocation::ABSENT_GIVE_POA;
        $form['attendance[mandataire]'] = $convocation2->getUser()->getId();

        $this->client->submit($form);

        $crawler->filter('h1')->children('div.badge-info')->count(1);

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $alert = $crawler->filter('section')->children('div.alert')->count();
        $this->assertSame(1, $alert);

        $this->assertNotEmpty($this->getOneEntityBy(Convocation::class, ['attendance' => 'poa']));
        //
    }



    public function testAttendanceIsAbsentReplacedByDeputy()
    {
        $convocation = ConvocationStory::convocationActor3SentWithToken();
        $user = UserStory::actorWithDeputy()->object();
        $deputy = $user->getDeputy();

        dump($deputy->getId());

        EmailTemplateStory::load();

        ConvocationFactory::createOne([
            'sitting' => $convocation->getSitting(),
            'user' => $deputy,
            'category' => Convocation::CATEGORY_DELEGATION,
        ]);

        AttendanceTokenFactory::createOne([
            'token' => 'mytoken',
            'convocation' => $convocation,
        ]);
        $token = $convocation->getAttendancetoken()->getToken();

        $this->loginAsUserMontpellier();
        $crawler = $this->client->request(
            Request::METHOD_GET,
            '/attendance/confirmation/' . $token
        );
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Merci de confirmer votre présence")');
        $this->assertCount(1, $item);

        $select = $crawler->filter('select[name="attendance[deputy]"]')->getNode(0);
        $select->appendChild(new \DOMElement('option', 'New Option 1'))->setAttribute('value', $deputy->getId());
        ;

        $form = $crawler->selectButton('Enregistrer')->form();
        $form['attendance[attendance]'] = Convocation::ABSENT_SEND_DEPUTY;
        $form['attendance[deputy]'] = $deputy->getId();

        $this->client->submit($form);

        $crawler->filter('h1')->children('div.badge-info')->count(1);

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $alert = $crawler->filter('section')->children('div.alert')->getNode(0)->textContent;

        $this->assertStringContainsString("remplacé par votre suppléant", $alert);

        $deputyConvo = $this->getOneEntityBy(Convocation::class, ['category' => 'delegation', 'user' => $deputy]);
        $this->assertNotEmpty($deputyConvo);
    }

    public function testAttendanceRedirect()
    {
        $convocation = ConvocationStory::convocationActor2SentWithToken();
        AttendanceTokenFactory::createOne([
            'token' => 'mytoken',
            'convocation' => $convocation,
        ]);
        $token = $convocation->getAttendancetoken()->getToken();

        $this->loginAsAdminLibriciel();
        $crawler = $this->client->request(
            Request::METHOD_GET,
            '/attendance/redirect/' . $token
        );
        $this->assertResponseStatusCodeSame(200);

        $crawler->filter('section')->children('div.alert')->count(1);
    }

    public function testAttendanceIsDeputy()
    {
        $user = UserFactory::createOne(['role' => RoleStory::actor()]);
        $deputy = UserFactory::createOne(["role" => RoleStory::deputy()]);

        $convocation = ConvocationFactory::createOne([
            'sitting' => SittingStory::sittingConseilWithTokenSent(),
            'user' => $user,
            'category' => Convocation::CATEGORY_CONVOCATION,
            'sentTimestamp' => null,
            'deputy' => $deputy
        ]);

        $invitation = ConvocationFactory::createOne([
            'sitting' => SittingStory::sittingConseilWithTokenSent(),
            'user' => $deputy,
            'category' => Convocation::CATEGORY_INVITATION,
            'sentTimestamp' => null,
        ]);

        AttendanceTokenFactory::createOne([
            'token' => 'mytoken',
            'convocation' => $invitation,
        ]);
        $token = $invitation->getAttendancetoken()->getToken();

        $this->loginAsUserMontpellier();
        $this->client->request(Request::METHOD_GET, '/attendance/confirmation/deputy/' . $token);
        $this->assertResponseStatusCodeSame(301);
        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('.alert')->text("Vous n'avez pas besoin de confirmer votre présence en tant que suppléant. Merci pour votre retour.");
        $this->assertSame("Vous n'avez pas besoin de confirmer votre présence en tant que suppléant. Merci pour votre retour.", $item);
    }
}
