<?php

namespace App\Tests\Controller\Easy;

use App\Controller\Easy\AttendanceController;
use App\Repository\ConvocationRepository;
use App\Tests\Factory\ConvocationFactory;
use App\Tests\Factory\SittingFactory;
use App\Tests\Factory\UserFactory;
use App\Tests\LoginTrait;
use App\Tests\Story\RoleStory;
use App\Tests\Story\UserStory;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class AttendanceControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;
    use LoginTrait;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }


    public function testIndex()
    {
        $actorLibriciel1 = UserStory::actorLibriciel1();

        $actorMandator = UserFactory::createOne([
            'structure' => $actorLibriciel1->getStructure(),
            'role' => RoleStory::actor()
        ]);

        $sitting = SittingFactory::createOne([
            'structure' => $actorLibriciel1->getStructure()
        ]);

        $convocationActorLibriciel = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => $actorLibriciel1
        ]);

        $convocationActorMandator = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => $actorMandator
        ]);

        $this->login($actorLibriciel1->getUsername());

        $crawler = $this->client->request('GET', '/attendance/' . $convocationActorLibriciel->getId());

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['attendance[attendance]'] = 'poa';
        $form['attendance[mandataire]'] = $actorMandator->getId();

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $convocationActorLibriciel->refresh();

        $this->assertSame("poa", $convocationActorLibriciel->getAttendance());
        $this->assertSame($actorMandator->getId(), $convocationActorLibriciel->getMandator()->getId());
    }
}
