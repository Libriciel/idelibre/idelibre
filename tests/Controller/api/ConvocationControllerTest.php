<?php

namespace App\Tests\Controller\api;

use App\Entity\Convocation;
use App\Service\Convocation\ConvocationManager;
use App\Tests\Factory\ConvocationFactory;
use App\Tests\Factory\EmailTemplateFactory;
use App\Tests\Factory\SittingFactory;
use App\Tests\Factory\TimestampFactory;
use App\Tests\Factory\TypeFactory;
use App\Tests\Factory\UserFactory;
use App\Tests\FileTrait;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use App\Tests\Story\ApiUserStory;
use App\Tests\Story\ConvocationStory;
use App\Tests\Story\EmailTemplateStory;
use App\Tests\Story\RoleStory;
use App\Tests\Story\SittingStory;
use App\Tests\Story\StructureStory;
use App\Tests\Story\TypeStory;
use App\Tests\Story\UserStory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ConvocationControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;
    use FindEntityTrait;
    use LoginTrait;
    use FileTrait;

    private ?KernelBrowser $client;
    private ObjectManager $entityManager;
    private ConvocationManager $convocationManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->convocationManager = self::getContainer()->get(ConvocationManager::class);

        self::ensureKernelShutdown();
        $this->client = static::createClient();

        UserStory::load();
        ConvocationStory::load();
        TypeStory::load();
        EmailTemplateStory::load();
        SittingStory::load();
        ApiUserStory::load();
    }

    public function testGetConvocations()
    {
        $sitting = SittingStory::sittingConseilLibriciel();

        $this->loginAsAdminLibriciel();
        $this->client->request(Request::METHOD_GET, '/api/convocations/' . $sitting->getId());
        $this->assertResponseStatusCodeSame(200);

        $convocations = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertCount(2, $convocations['actors']);
        $this->assertCount(0, $convocations['employees']);
        $this->assertCount(0, $convocations['guests']);
    }

    public function testSendConvocation()
    {
        $sitting = SittingStory::sittingConseilLibriciel();
        UserStory::actorLibriciel1();
        EmailTemplateStory::emailTemplateConseilLs()->object();
        $convocation = ConvocationStory::convocationActor1();

        $this->loginAsAdminLibriciel();
        $bag = static::getContainer()->getParameterBag();
        $year = $sitting->getDate()->format('Y');
        $tokenPath = "{$bag->get('token_directory')}{$sitting->getStructure()->getId()}/$year/{$sitting->getId()}";



        $this->client->request(Request::METHOD_POST, '/api/convocations/' . $convocation->getId() . '/send');
        $this->assertResponseStatusCodeSame(200);
        $this->assertNotEmpty($convocation->getSentTimestamp());

        $bag = static::getContainer()->getParameterBag();
        $year = $sitting->getDate()->format('Y');

        $tokenPath = "{$bag->get('token_directory')}{$sitting->getStructure()->getId()}/$year/{$sitting->getId()}";
        $this->assertEquals(2, $this->countFileInDirectory($tokenPath));
    }

    public function testSetAttendanceNoLogin()
    {
        $this->client->request(
            Request::METHOD_POST,
            '/api/convocations/attendance',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );
        $this->assertResponseStatusCodeSame(302);
    }

    public function testSetAttendance()
    {
        $convocation = ConvocationStory::convocationActor1()->object();

        $data = [['convocationId' => $convocation->getId(), 'attendance' => 'absent', 'isRemote' => false]];

        $this->loginAsAdminLibriciel();

        $this->client->request(
            Request::METHOD_POST,
            '/api/convocations/attendance',
            [],
            [],
            [],
            json_encode($data)
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('absent', $convocation->getAttendance());
        $this->assertEquals(null, $convocation->getDeputy());
    }


    public function testSetAttendanceConvocationNotExists()
    {
        $randomUUID = 'ce854a57-0e0b-459e-b93e-53239680b30e';

        $data = [['convocationId' => $randomUUID, 'attendance' => 'absent', 'deputy' => 'John Doe', 'isRemote' => false]];

        $this->loginAsAdminLibriciel();

        $this->client->request(
            Request::METHOD_POST,
            '/api/convocations/attendance',
            [],
            [],
            [],
            json_encode($data)
        );
        $this->assertResponseStatusCodeSame(403);
    }


    public function testReSendConvocation()
    {
        $sentDate = new \DateTime("-1 year");

        $structure = StructureStory::libriciel();

        $type = TypeFactory::createOne([
            'structure' => $structure
        ]);

        $emailTemplate = EmailTemplateFactory::createOne([
            'structure' => $structure,
            'type' => $type
        ]);


        $sitting = SittingFactory::createOne([
            'structure' => $structure,
            'type' => $type
        ]);

        $actor = UserFactory::createOne([
            'role' => RoleStory::actor()
        ]);

        $sentTimestamp = TimestampFactory::createOne();
        $sentTimestamp->_set("createdAt", $sentDate)->_save();

        $convocation = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => $actor,
            'sentTimestamp' => $sentTimestamp
        ]);

        $this->loginAsAdminLibriciel();

        $this->client->request(Request::METHOD_POST, '/api/convocations/' . $convocation->getId() . '/send');
        $this->assertResponseStatusCodeSame(200);
        $this->assertNotEmpty($convocation->getSentTimestamp());

        $bag = static::getContainer()->getParameterBag();
        $year = $sitting->getDate()->format('Y');

        $tokenPath = "{$bag->get('token_directory')}{$sitting->getStructure()->getId()}/$year/{$sitting->getId()}";
        $this->assertEquals(2, $this->countFileInDirectory($tokenPath));

        $this->assertSame($sentDate->getTimestamp(), $convocation->getSentTimestamp()->getCreatedAt()->getTimestamp());
    }

    public function testResetDeputyAttendance()
    {
        $sitting = SittingStory::sittingConseilLibriciel();

        $user = UserFactory::createOne([
            'role' => RoleStory::deputy()
        ]);
        $convocation = ConvocationFactory::createOne([
            'attendance' => Convocation::PRESENT,
            'user' => $user,
            'sitting' => $sitting
        ]);

        $this->loginAsSecretaryLibriciel();

        $this->client->request(Request::METHOD_GET, '/api/convocation/' . $convocation->getId() . '/resetDeputyAttendance');
        $this->assertResponseStatusCodeSame(200);

        $this->assertEquals(Convocation::ABSENT, $convocation->getAttendance());
    }

    public function testGetDeputyAvailable()
    {
        $structure = StructureStory::libriciel();
        $convocation = ConvocationStory::convocationActor1()->object();

        $user = UserFactory::createOne([
            'role' => RoleStory::actor(),
            'structure' => $structure
        ]);

        $deputyAvailable = UserFactory::createOne([
            'role' => RoleStory::deputy(),
            'structure' => $structure
        ]);

        UserFactory::createOne([
            'role' => RoleStory::deputy(),
            'titular' => $user,
            'structure' => $structure
        ]);

        $this->loginAsSecretaryLibriciel();

        $this->client->request(Request::METHOD_GET, '/api/convocation/' . $convocation->getId() . '/deputiesAvailable');
        $this->assertResponseStatusCodeSame(200);

        $deputies = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('deputiesAvailable', $deputies);
        $this->assertCount(1, $deputies['deputiesAvailable']);
        $this->assertEquals($deputyAvailable->getId(), $deputies['deputiesAvailable'][0]['id']);
    }
}
