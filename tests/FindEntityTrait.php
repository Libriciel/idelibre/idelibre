<?php

namespace App\Tests;

use App\Entity\Annex;
use App\Entity\ApiUser;
use App\Entity\Convocation;
use App\Entity\EventLog\EventLog;
use App\Entity\ForgetToken;
use App\Entity\Group;
use App\Entity\Party;
use App\Entity\Project;
use App\Entity\Role;
use App\Entity\Sitting;
use App\Entity\Structure;
use App\Entity\Theme;
use App\Entity\Type;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

trait FindEntityTrait
{
    public function getOneEntityBy($entityClass, array $criteria)
    {
        $entityManager = self::getcontainer()->get(EntityManagerInterface::class);

        $repository = $entityManager->getRepository($entityClass);

        return $repository->findOneBy($criteria);
    }

    public function getOneSittingBy(array $criteria): ?Sitting
    {
        return $this->getOneEntityBy(Sitting::class, $criteria);
    }

    public function getOneProjectBy(array $criteria): ?Project
    {
        return $this->getOneEntityBy(Project::class, $criteria);
    }

    public function getOneAnnexBy(array $criteria): ?Annex
    {
        return $this->getOneEntityBy(Annex::class, $criteria);
    }

    public function getOneTypeBy(array $criteria): ?Type
    {
        return $this->getOneEntityBy(Type::class, $criteria);
    }

    public function getOneUserBy(array $criteria): ?User
    {
        return $this->getOneEntityBy(User::class, $criteria);
    }

    public function getOneConvocationBy(array $criteria): ?Convocation
    {
        return $this->getOneEntityBy(Convocation::class, $criteria);
    }

    public function getOneStructureBy(array $criteria): ?Structure
    {
        return $this->getOneEntityBy(Structure::class, $criteria);
    }

    public function getOneGroupBy(array $criteria): ?Group
    {
        return $this->getOneEntityBy(Group::class, $criteria);
    }

    public function getOneApiUserBy(array $criteria): ?ApiUser
    {
        return $this->getOneEntityBy(ApiUser::class, $criteria);
    }

    public function getOnePartyBy(array $criteria): ?Party
    {
        return $this->getOneEntityBy(Party::class, $criteria);
    }

    public function getOneRoleBy(array $criteria): ?Role
    {
        return $this->getOneEntityBy(Role::class, $criteria);
    }

    public function getOneThemeBy(array $criteria): ?Theme
    {
        return $this->getOneEntityBy(Theme::class, $criteria);
    }

    public function getOneForgetTokenBy(array $criteria): ?ForgetToken
    {
        return $this->getOneEntityBy(ForgetToken::class, $criteria);
    }


    public function getOneEventLog(array $criteria): ?EventLog
    {
        return $this->getOneEntityBy(EventLog::class, $criteria);
    }
}
