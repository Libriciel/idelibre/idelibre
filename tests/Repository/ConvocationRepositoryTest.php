<?php

namespace App\Tests\Repository;

use App\Entity\Convocation;
use App\Entity\User;
use App\Repository\ConvocationRepository;
use App\Tests\Factory\ConvocationFactory;
use App\Tests\Factory\SittingFactory;
use App\Tests\Factory\StructureFactory;
use App\Tests\Factory\UserFactory;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use App\Tests\Story\SittingStory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ConvocationRepositoryTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;
    use FindEntityTrait;
    use LoginTrait;

    private ?KernelBrowser $client;
    private ObjectManager $entityManager;
    private ConvocationRepository $convocationRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->convocationRepository = self::getContainer()->get(ConvocationRepository::class);

        self::ensureKernelShutdown();
        $this->client = static::createClient();
    }

    public function testGetConvocationsBySittingAndActorIds()
    {
        $sitting = SittingStory::sittingConseilLibriciel()->object();
        $user1 = UserFactory::createOne();
        $user2 = UserFactory::createOne();

        ConvocationFactory::createOne(['user' => $user1, 'sitting' => $sitting]);
        ConvocationFactory::createOne(['user' => $user2, 'sitting' => $sitting]);

        $convocations = $this->convocationRepository->getConvocationsBySittingAndActorIds($sitting, [$user1->getId(), $user2->getId()]);

        $this->assertCount(2, $convocations);
    }

    public function testGetConvocationWithUser()
    {
        $sitting = SittingStory::sittingConseilLibriciel()->object();
        $all_convocations = ConvocationFactory::createMany(5, ['sitting' => $sitting]);

        $convocationsId = [];
        foreach ($all_convocations as $convocation) {
            $convocationsId[] = $convocation->getId();
        }

        $convocations = $this->convocationRepository->getConvocationsWithUser($convocationsId);

        $this->assertCount(5, $convocations);
    }

    public function testGetConvocationsWithUserBySitting()
    {
        $sitting = SittingStory::sittingConseilLibriciel()->object();
        $user1 = UserFactory::createOne();
        $user2 = UserFactory::createOne();

        ConvocationFactory::createOne(['user' => $user1, 'sitting' => $sitting]);
        ConvocationFactory::createOne(['user' => $user2, 'sitting' => $sitting]);

        $convocations = $this->convocationRepository->getConvocationsWithUserBySitting($sitting);

        $this->assertCount(2, $convocations);
    }

    public function testGetAndDeleteInvitationBySitting(): void
    {
        $sitting = SittingFactory::createOne(["structure" => StructureFactory::createOne()]);
        ConvocationFactory::createOne([
            "category" => Convocation::CATEGORY_INVITATION,
            "sitting" => $sitting,
            "user" => UserFactory::createOne()
        ]);
        ConvocationFactory::createOne([
            "category" => Convocation::CATEGORY_CONVOCATION,
            "sitting" => $sitting,
            "user" => UserFactory::createOne()
        ]);

        $this->convocationRepository->deleteInvitationsBySitting($sitting->object());
        $convocations = $this->convocationRepository->findAll();

        $this->assertCount(1, $convocations);
        $this->assertSame(Convocation::CATEGORY_CONVOCATION, $convocations[0]->getCategory());
    }

    public function testFindPresentAndRemoteUserInSitting()
    {
        $sitting = SittingFactory::createOne([
            "structure" => StructureFactory::createOne()
        ])->object();

        ConvocationFactory::createMany(2, [
            "sitting" => $sitting,
            "attendance" => Convocation::ABSENT,
            "user" => UserFactory::new(),
        ]);
        ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::ABSENT_SEND_DEPUTY,
            "deputy" => UserFactory::createOne()
        ])->object();

        $availableUser1 = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::PRESENT,
            "user" => UserFactory::createOne(["lastName" => "Abby"]),
            "deputy" => null,
            "mandator" => null
        ])->object();

        $availableUser2 = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::PRESENT,
            "user" => UserFactory::createOne(["lastName" => "Bobby"]),
            "deputy" => null,
            "mandator" => null
        ])->object();

        $mandatorConvocation = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::PRESENT,
            "user" => UserFactory::createOne(["lastName" => "Mandator"]),
            "mandator" => null,
            "deputy" => null
        ])->object();

        $convocationWithMandator = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "user" => UserFactory::new(["lastName" => "hasMandator"]),
            "attendance" => Convocation::ABSENT_GIVE_POA,
            "mandator" => $mandatorConvocation->getUser(),
            "deputy" => null
        ])->object();

        $userConvocation = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::REMOTE,
            "user" => UserFactory::new(["lastName" => "MyUser"])
        ])->object();

        $user = $userConvocation->getUser();
        $toExclude = [$convocationWithMandator->getMandator()->getId()];

        $potentialPoaReceivers = $this->convocationRepository->findAvailableMandatorsInSitting($sitting, $user, $toExclude)->getQuery()->getResult();

        $this->assertCount(2, $potentialPoaReceivers);
        $this->assertSame($availableUser1->getId(), $potentialPoaReceivers[0]->getId());
        $this->assertSame($availableUser2->getId(), $potentialPoaReceivers[1]->getId());
    }

    public function testFindPresentAndRemoteUserInSittingIfNoMandator()
    {
        $sitting = SittingFactory::createOne([
            "structure" => StructureFactory::createOne()
        ])->object();

        $availableUser1 = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::PRESENT,
            "user" => UserFactory::createOne(["lastName" => "Abby"]),
            "deputy" => null,
            "mandator" => null
        ])->object();

        $availableUser2 = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::PRESENT,
            "user" => UserFactory::createOne(["lastName" => "Bobby"]),
            "deputy" => null,
            "mandator" => null
        ])->object();


        $userConvocation = ConvocationFactory::createOne([
            "sitting" => $sitting,
            "attendance" => Convocation::REMOTE,
            "user" => UserFactory::new()
        ])->object();

        $user = $userConvocation->getUser();
        $toExclude = [];

        $potentialPoaReceivers = $this->convocationRepository->findAvailableMandatorsInSitting($sitting, $user, $toExclude)->getQuery()->getResult();
        $this->assertCount(2, $potentialPoaReceivers);
        $this->assertSame($availableUser1->getId(), $potentialPoaReceivers[0]->getId());
        $this->assertSame($availableUser2->getId(), $potentialPoaReceivers[1]->getId());
    }
}
