<?php

namespace App\Tests\Service\Convocation;

use App\Entity\Convocation;
use App\Service\Convocation\ConvocationManager;
use App\Tests\Factory\ConvocationFactory;
use App\Tests\Factory\SittingFactory;
use App\Tests\Factory\StructureFactory;
use App\Tests\Factory\TimestampFactory;
use App\Tests\Factory\UserFactory;
use App\Tests\Story\FileStory;
use App\Tests\Story\RoleStory;
use App\Tests\Story\SittingStory;
use App\Tests\Story\StructureStory;
use App\Tests\Story\UserStory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ConvocationManagerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;

    private ?KernelBrowser $client;
    private ObjectManager $entityManager;
    private ConvocationManager $convocationManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->convocationManager = self::getContainer()->get(ConvocationManager::class);

        self::ensureKernelShutdown();
    }

    public function testKeepOnlyNotSent()
    {
        $timestamp = TimestampFactory::createOne();
        $sitting = SittingStory::sittingConseilLibriciel();
        $sent_convocations = ConvocationFactory::createMany(1, [
            'sitting' => $sitting,
            'sentTimestamp' => $timestamp,
        ]);
        $not_sent_convocation = ConvocationFactory::createMany(2, [
            'sitting' => $sitting,
            'sentTimestamp' => null,
        ]);

        $all_convocations = [...$sent_convocations, ...$not_sent_convocation];

        $convocations = $this->convocationManager->keepOnlyNotSent($all_convocations);

        $this->assertCount(2, $convocations);
    }

    public function testCountConvocationNotAnswered()
    {
        $sitting = SittingStory::sittingConseilLibriciel();
        $gestionnaire = UserFactory::createOne([
            'role' => RoleStory::secretary(),
        ]);
        $user = UserFactory::createOne(["deputy" => UserFactory::createOne(["role" => RoleStory::deputy()])]);
        $convocation1 = ConvocationFactory::createOne(["sitting" => $sitting, "attendance" => ""]);
        $convocation2 = ConvocationFactory::createOne(["sitting" => $sitting, "attendance" => Convocation::PRESENT]);
        $convocation3 = ConvocationFactory::createOne(["sitting" => $sitting, "attendance" => Convocation::PRESENT, 'user' => $gestionnaire]);
        $convocation4 = ConvocationFactory::createOne(["sitting" => $sitting, "attendance" => "", 'user' => $user->getDeputy()]);

        $convocations = [$convocation1->object(), $convocation2->object(), $convocation3->object(), $convocation4->object()];

        $countConvocationNotAnswered = $this->convocationManager->countConvocationNotanswered($convocations);
        $this->assertIsInt($countConvocationNotAnswered);
        $this->assertEquals(1, $countConvocationNotAnswered);
    }

    public function testGetAlreadyMandatorBySitting()
    {
        $sitting = SittingFactory::createOne(["structure" => StructureFactory::createOne()]);
        $convocationNoMandators = ConvocationFactory::createMany(4, ["sitting" => $sitting, 'attendance' => Convocation::PRESENT]);
        $convocationWithMandators = ConvocationFactory::createMany(2, ["sitting" => $sitting, 'attendance' => Convocation::ABSENT_GIVE_POA, 'mandator' => UserFactory::createOne()]);

        $convocations = [...$convocationNoMandators, ...$convocationWithMandators];

        $mandatorsList = $this->convocationManager->getAlreadyMandatorBySitting($convocations);
        $this->assertIsArray($mandatorsList);
        $this->assertCount(2, $mandatorsList);
        $this->assertEquals($convocationWithMandators[0]->getMandator()->getId(), $mandatorsList[0]);
        $this->assertEquals($convocationWithMandators[1]->getMandator()->getId(), $mandatorsList[1]);
    }

    public function testGetQuorum()
    {
        $sitting = SittingFactory::createOne([
            "name" => "Conseil Libriciel",
            "structure" => StructureStory::libriciel(),
            'date' => new \DateTime('2020-10-22'),
            'convocationFile' => FileStory::fileConvocation(),
            'isRemoteAllowed' => true,
            'isMandatorAllowed' => true,
        ]);

        ConvocationFactory::createMany(4, ["attendance" => Convocation::PRESENT, "sitting" => $sitting]);
        ConvocationFactory::createMany(2, ["attendance" => Convocation::ABSENT, "sitting" => $sitting]);
        ConvocationFactory::createMany(2, ["attendance" => Convocation::ABSENT_SEND_DEPUTY, "sitting" => $sitting]);
        ConvocationFactory::createMany(2, ["attendance" => Convocation::REMOTE, "sitting" => $sitting]);

        $expected = "8/10";

        $quorum = $this->convocationManager->getQuorum($sitting->_real());

        $this->assertSame($expected, $quorum);
    }

    public function testGetInTheRoom()
    {
        $sitting = SittingFactory::createOne([
            "name" => "Conseil Libriciel",
            "structure" => StructureStory::libriciel(),
            'date' => new \DateTime('2020-10-22'),
            'convocationFile' => FileStory::fileConvocation(),
            'isRemoteAllowed' => true,
            'isMandatorAllowed' => true,
        ]);

        ConvocationFactory::createMany(4, ["attendance" => Convocation::PRESENT, "sitting" => $sitting]);
        ConvocationFactory::createMany(2, ["attendance" => Convocation::ABSENT, "sitting" => $sitting]);
        ConvocationFactory::createMany(2, ["attendance" => Convocation::ABSENT_SEND_DEPUTY, "sitting" => $sitting]);
        ConvocationFactory::createMany(2, ["attendance" => Convocation::REMOTE, "sitting" => $sitting]);

        $expected = "4/10";

        $inTheRoom = $this->convocationManager->getInTheRoom($sitting->_real());

        $this->assertSame($expected, $inTheRoom);
    }

    public function testGetPoa()
    {
        $sitting = SittingFactory::createOne([
            "name" => "Conseil Libriciel",
            "structure" => StructureStory::libriciel(),
            'date' => new \DateTime('2020-10-22'),
            'convocationFile' => FileStory::fileConvocation(),
            'isRemoteAllowed' => true,
            'isMandatorAllowed' => true,
        ]);

        ConvocationFactory::createMany(4, ["attendance" => Convocation::PRESENT, "sitting" => $sitting]);
        ConvocationFactory::createMany(2, ["attendance" => Convocation::ABSENT_GIVE_POA, "sitting" => $sitting]);

        $expected = "2/6";

        $inTheRoom = $this->convocationManager->getPoa($sitting->_real());

        $this->assertSame($expected, $inTheRoom);
    }

    public function testGetAssignedandPresentDeputy()
    {
        $sitting = SittingFactory::createOne([
            "name" => "Conseil Libriciel",
            "structure" => StructureStory::libriciel(),
            'date' => new \DateTime('2020-10-22'),
            'convocationFile' => FileStory::fileConvocation(),
            'isRemoteAllowed' => true,
            'isMandatorAllowed' => true,
        ]);

        $deputy1 = UserFactory::createOne([
            "structure" => $sitting->getStructure(),
            "role" => RoleStory::deputy(),
        ]);

        $deputy2 = UserFactory::createOne([
            "structure" => $sitting->getStructure(),
            "role" => RoleStory::deputy(),
        ]);

        ConvocationFactory::createMany(2, ["attendance" => Convocation::ABSENT_SEND_DEPUTY, "sitting" => $sitting, "category" => Convocation::CATEGORY_CONVOCATION]);
        ConvocationFactory::createOne(["attendance" => '', "sitting" => $sitting, "category" => Convocation::CATEGORY_DELEGATION, "user" => $deputy1->_real()]);
        ConvocationFactory::createOne(["attendance" => '', "sitting" => $sitting, "category" => Convocation::CATEGORY_DELEGATION, "user" => $deputy2->_real()]);

        $expected = "2/2";

        $presents = $this->convocationManager->getAssignedandPresentDeputy($sitting->_real());

        $this->assertSame($expected, $presents);
    }

    public function testGetDeputyToExclude()
    {
        $sitting = SittingStory::sittingConseilLibriciel();
        $actor = UserStory::actorWithDeputy();
        $convocation = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => $actor,
            'attendance' => Convocation::PRESENT
        ]);

        $toExclude = $this->convocationManager->getDeputiestoExclude($convocation->_real());

        $this->assertIsArray($toExclude);
        $this->assertCount(1, $toExclude);
        $this->assertEquals($actor->getDeputy()->getId(), $toExclude[0]->getId());
    }

    public function testNewlyAssignedDeputies()
    {
        $sitting = SittingStory::sittingConseilLibriciel();
        $actor = UserStory::actorWithDeputy();
        ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => $actor,
            'attendance' => Convocation::ABSENT_SEND_DEPUTY,
            "deputy" => $actor->getDeputy()
        ]);

        $deputies = $this->convocationManager->newlyAssignedDeputies($sitting->_real());

        $this->assertIsArray($deputies);
        $this->assertCount(1, $deputies);
        $this->assertEquals($actor->getDeputy()->getId(), $deputies[0]->getId());
    }

    public function testFindPreviousDeputyIfAny()
    {
        $sitting = SittingStory::sittingConseilLibriciel();
        $actor = UserStory::actorWithDeputy()->_real();
        $convocation = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => $actor,
            'attendance' => Convocation::ABSENT_SEND_DEPUTY,
            "deputy" => $actor->getDeputy()
        ])->_real();

        $previousDeputy = $this->convocationManager->findPreviousDeputyIfAny($convocation);

        $this->assertEquals($convocation->getDeputy()->getId(), $previousDeputy->getId());
    }

    public function testFindPreviousDeputyIfAnyNull()
    {
        $sitting = SittingStory::sittingConseilLibriciel();
        $actor = UserStory::actorLibriciel1()->_real();
        $convocation = ConvocationFactory::createOne([
            'sitting' => $sitting,
            'user' => $actor,
            'attendance' => Convocation::ABSENT_SEND_DEPUTY,
            "deputy" => null
        ])->_real();

        $previousDeputy = $this->convocationManager->findPreviousDeputyIfAny($convocation);

        $this->assertNull($previousDeputy);
    }
}
