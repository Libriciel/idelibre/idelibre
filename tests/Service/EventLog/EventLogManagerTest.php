<?php

declare(strict_types=1);

namespace App\Tests\Service\EventLog;

use App\Entity\EventLog\Action;
use App\Repository\EventLogRepository;
use App\Service\EventLog\EventLogManager;
use App\Tests\Factory\SittingFactory;
use App\Tests\Factory\StructureFactory;
use App\Tests\Factory\UserFactory;
use App\Tests\LoginTrait;
use App\Tests\Story\StructureStory;
use App\Tests\Story\UserStory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class EventLogManagerTest extends WebTestCase
{
    use Factories;
    use ResetDatabase;
    use LoginTrait;

    private EventLogManager $eventLogManager;
    private EventLogRepository $eventLogRepository;
    private ObjectManager $entityManager;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $container = self::getContainer();
        $this->eventLogManager = $container->get(EventLogManager::class);
        $this->eventLogRepository = $container->get(EventLogRepository::class);

        self::ensureKernelShutdown();
        $this->client = static::createClient();

        UserStory::load();
    }

    public function testCreateLogIfUserCreated(): void
    {
        $this->loginAsSuperAdmin();

        $user = UserFactory::createOne(["structure" => StructureStory::libriciel()]);

        $this->eventLogManager->createLog(Action::USER_CREATE, $user->getId(), $user->getUsername(), $user->getStructure()->getId(), true);
        $eventLog = $this->eventLogRepository->findOneBy(["targetId" => $user->getId()]);
        $this->assertNotNull($eventLog);
        $this->assertEquals(Action::USER_CREATE, $eventLog->getAction());
        $this->assertEquals($user->getId(), $eventLog->getTargetId());
        $this->assertEquals($user->getUsername(), $eventLog->getTargetName());
        $this->assertEquals($user->getStructure()->getId(), $eventLog->getStructureId());
        $this->assertEquals("superadmin", $eventLog->getAuthorName());
    }

    public function testCreateLogIfUserDeleted(): void
    {
        $this->loginAsSuperAdmin();

        $user = UserFactory::createOne(["structure" => StructureStory::libriciel()])->object();

        $this->client->request(Request::METHOD_DELETE, '/user/delete/' . $user->getId());
        $this->eventLogManager->createLog(Action::USER_DELETE, $user->getId(), $user->getUsername(), $user->getStructure()->getId(), true);
        $eventLog = $this->eventLogRepository->findOneBy(["action" => Action::USER_DELETE]);
        $this->assertNotNull($eventLog);
        $this->assertEquals(Action::USER_DELETE, $eventLog->getAction());
        $this->assertEquals($user->getId(), $eventLog->getTargetId());
        $this->assertEquals($user->getUsername(), $eventLog->getTargetName());
        $this->assertEquals($user->getStructure()->getId(), $eventLog->getStructureId());
    }

    public function testCreateLogSittingDelete(): void
    {
        $this->loginAsSuperAdmin();
        $sitting = SittingFactory::createOne(["structure" => StructureStory::libriciel()])->object();
        $this->client->request(Request::METHOD_DELETE, '/sitting/delete/' . $sitting->getId());
        $this->eventLogManager->createLog(Action::SITTING_DELETE, $sitting->getId(), $sitting->getName(), $sitting->getStructure()->getId(), true);
        $eventLog = $this->eventLogRepository->findOneBy(["action" => Action::SITTING_DELETE]);
        $this->assertNotNull($eventLog);
        $this->assertEquals(Action::SITTING_DELETE, $eventLog->getAction());
        $this->assertEquals($sitting->getId(), $eventLog->getTargetId());
        $this->assertEquals($sitting->getName(), $eventLog->getTargetName());
        $this->assertEquals($sitting->getStructure()->getId(), $eventLog->getStructureId());
    }

    public function testCreateLogIfSittingArchived()
    {
        $this->loginAsSuperAdmin();

        $sitting = SittingFactory::createOne([
            "structure" => StructureStory::libriciel(),
            "isArchived" => true
            ])->object();

        $this->eventLogManager->createLog(Action::SITTING_ARCHIVED, $sitting->getId(), $sitting->getName(), $sitting->getStructure()->getId(), true);
        $eventLog = $this->eventLogRepository->findOneBy(["action" => Action::SITTING_ARCHIVED]);
        $this->assertNotNull($eventLog);
        $this->assertEquals(Action::SITTING_ARCHIVED, $eventLog->getAction());
        $this->assertEquals($sitting->getId(), $eventLog->getTargetId());
        $this->assertEquals($sitting->getName(), $eventLog->getTargetName());
        $this->assertEquals($sitting->getStructure()->getId(), $eventLog->getStructureId());
    }
}
