<?php

namespace App\Tests\Service\Report;

use App\Service\File\Generator\FileCheckerException;
use App\Service\File\Generator\UnsupportedExtensionException;
use App\Service\Report\ArchiverException;
use App\Service\Report\ArchiverManager;
use App\Service\Report\CsvSittingReport;
use App\Tests\LoginTrait;
use App\Tests\Story\SittingStory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ArchiverManagerTest extends WebTestCase
{
    use Factories;
    use ResetDatabase;

    private ?KernelBrowser $client;
    private ArchiverManager $archiverManager;

    protected function setUp(): void
    {
        $this->archiverManager = self::getContainer()->get(ArchiverManager::class);
        $this->bag = self::getContainer()->get('parameter_bag');
    }

    /**
     * @throws UnsupportedExtensionException
     * @throws SyntaxError
     * @throws FileCheckerException
     * @throws ArchiverException
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function testGenerateFinalAndCopy()
    {
        $sitting = SittingStory::sittingConseilLibriciel()->object();

        $pathpdf = $this->bag->get('document_full_pdf_directory');
        $pathCsv = $this->bag->get('document_full_csv_directory');

        $this->archiverManager->generateFinalAndCopy($sitting);

        $this->assertNotEmpty($pathpdf . $sitting->getStructure()->getId() . '/' . $sitting->getId() . '_report_pdf_sitting.pdf');
        $this->assertNotEmpty($pathCsv . $sitting->getStructure()->getId() . '/' . $sitting->getId() . '_report_csv_sitting.csv');
    }
}
