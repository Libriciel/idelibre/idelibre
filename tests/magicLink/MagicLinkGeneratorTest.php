<?php

namespace App\Tests\magicLink;

use App\magicLink\MagicLinkGenerator;
use App\Service\Jwt\JwtManager;
use App\Tests\Factory\SittingFactory;
use App\Tests\Factory\UserFactory;
use App\Tests\Story\RoleStory;
use App\Tests\Story\StructureStory;
use Firebase\JWT\JWK;
use Libriciel\LshorodatageApiWrapper\LshorodatageInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Routing\RouterInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class MagicLinkGeneratorTest extends KernelTestCase
{
    use resetDatabase, factories;


    public function testGenerateEmployee()
    {
        $user = UserFactory::new()->create([
            'role' => RoleStory::employee(),
            'structure' => StructureStory::libriciel()
        ]);

        $sitting = SittingFactory::new()->create([
            'date' => new \DateTime(),
            'structure' => StructureStory::libriciel()
        ]);

        self::bootKernel();

        $magicLinkGenerator = self::getContainer()->get(MagicLinkGenerator::class);


        $magicLink = $magicLinkGenerator->generate($user->object(), $sitting->object());

        $this->assertNotEmpty($magicLink);

        parse_str(parse_url($magicLink)['query'], $params);

        $JwtManager = self::getContainer()->get(JwtManager::class);

        $decoded = $JwtManager->decode($params['token']);

        $this->assertTrue($decoded['isAuthorizedMagicLink']);
    }


    public function testGenerateAdmin()
    {
        $user = UserFactory::new()->create([
            'role' => RoleStory::admin(),
            'structure' => StructureStory::libriciel()
        ]);

        $sitting = SittingFactory::new()->create([
            'date' => new \DateTime(),
            'structure' => StructureStory::libriciel()
        ]);

        self::bootKernel();

        $magicLinkGenerator = self::getContainer()->get(MagicLinkGenerator::class);


        $magicLink = $magicLinkGenerator->generate($user->object(), $sitting->object());

        $this->assertNotEmpty($magicLink);

        parse_str(parse_url($magicLink)['query'], $params);

        $JwtManager = self::getContainer()->get(JwtManager::class);

        $decoded = $JwtManager->decode($params['token']);

        $this->assertFalse($decoded['isAuthorizedMagicLink']);
    }


    public function testGenerateSecretary()
    {
        $user = UserFactory::new()->create([
            'role' => RoleStory::secretary(),
            'structure' => StructureStory::libriciel()
        ]);

        $sitting = SittingFactory::new()->create([
            'date' => new \DateTime(),
            'structure' => StructureStory::libriciel()
        ]);

        self::bootKernel();

        $magicLinkGenerator = self::getContainer()->get(MagicLinkGenerator::class);


        $magicLink = $magicLinkGenerator->generate($user->object(), $sitting->object());

        $this->assertNotEmpty($magicLink);

        parse_str(parse_url($magicLink)['query'], $params);

        $JwtManager = self::getContainer()->get(JwtManager::class);

        $decoded = $JwtManager->decode($params['token']);

        $this->assertFalse($decoded['isAuthorizedMagicLink']);
    }
}
